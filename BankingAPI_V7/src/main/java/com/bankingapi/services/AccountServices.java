package com.bankingapi.services;

import java.util.List;

import com.bankingapi.model.Account;

public interface AccountServices {


	public String isAccountCreated(int userId, Account account);
	
	public List<Account> getAllAccounts();
	public Account getAccountByAccountId(int accountId);
	public List<Account> getAllAccountsByStatus(int statusId);
	public List<Account> getAccountsByUserId(int userId);
	
	public List<Account> getAccountsByUserIdAndAccountType(int userId, String accountType);
	
	public boolean hasEnoughBalanceForWithdrawal(int accountId, double balance);
	public String isMakeAWithdrawal(int accountId, double amount);
	public boolean makeAWithdrawal(int accountId, double amount);
	public String isMakeADeposit(int accountId, double amount);
	public boolean makeADeposit(int accountId, double amount);
	public String isTransferFunds(int sourceAccountId, int targetAccountId, double amount);
	public boolean makeTransferFunds(int sourceAccountId, int targetAccountId, double amount);
	
	public int getAccountIdByUserId(int userId);
	public int getUserIdInAccountByAccountId(int accountId);
	public boolean isOpenAccount(int accountId, int userId);
	public boolean isOpenAccount(int accountId);
	public boolean isPendingAccount(int accountId);
	public boolean isAccountOwnedByUser(int accountId, int userId);
	public boolean hasAccount(int accountId);
	public int getLastAccountId(int userId);
	
	public boolean hasEnoughOpenAccounts(int userId);
	public boolean hasEnoughOpenAccountsForTransferMoneyInYourAccounts(int userId);
	public boolean isEqualAccountId(int firstAccount, int secondAccount);
	
	public String isAccountUpdated(int userId, Account account);
	
	public boolean isValidStatusId(int statusId);
	public boolean isValidStatus(String status);
	public boolean isMatchStatusIdAndStatus(int statusId, String status);
	public boolean isValidTypeId(int typeId);	
	public boolean isValidType(String type);
	public boolean isMatchTypeIdAndType(int typeId, String type);
	
	public int numOfSavingsAccountbyaccruedInterest(int numOfMonths);
	public boolean applyInterestforSavingsAccount(int numOfMonths, double interestRate);
	
	public boolean isMatchUserIdAndAccountId(int userId, int accountId);
	public List<Account> getAccruedSavingsAccounts(int numOfMonths);	

	
}
