package com.bankingapi.servlethelper;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.controller.AccountController;
import com.bankingapi.controller.TransactionController;
import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.services.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class AccountRequestHelper {

	private static UserServices userServices = new UserServicesImpl();	
	
	public static void getProcess(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();
		
		if (session.getAttribute("userId") == null || !req.getMethod().equals("GET")) {
		
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;			
		}		
		
		switch(req.getRequestURI()) {
		
		case "/BankingAPI_V7/accounts":
			AccountController.getAllAccounts(req, resp);
			return;
		
		case "/BankingAPI_V7/accounts/":
			AccountController.getAccountById(req, resp);
			return;
		
		case "/BankingAPI_V7/accounts/status/":			
			AccountController.getAccountByStatus(req, resp);
			return;
	
		case "/BankingAPI_V7/accounts/owner/":
			
			String accountType=null;
			
			try {
				accountType = req.getParameter("accountType");
			} catch (Exception e) {
				System.out.println("Wrong accountType");
			}
			
			if(accountType == null) {
				System.out.println("I am in by owner");
				AccountController.getAccountByOwner(req, resp);
			} else {
				System.out.println("I am in by owner and by type");
				AccountController.getAcccountByUserIdAndType(req, resp);
			}
			
			return;
			
		default: 
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
	
	public static void postProcess(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		HttpSession session = req.getSession();
		
		if (session.getAttribute("userId") == null || !req.getMethod().equals("POST")) {
			
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;			
		}	
		
		switch(req.getRequestURI()) {
		
		case "/BankingAPI_V7/accounts":
			AccountController.submitAccount(req, resp);
			return;
			
		case "/BankingAPI_V7/accounts/withdrawal":
			TransactionController.withdrawal(req, resp);
			return;
		
		case "/BankingAPI_V7/accounts/deposit":
			TransactionController.deposit(req, resp);
			return;
		
		case "/BankingAPI_V7/accounts/transfer":			
			TransactionController.transfer(req, resp);
			return;
			
		case "/BankingAPI_V7/accounts/passtime":			
			AccountController.passTime(req, resp);
			return;
			
		default: 
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}

	}
	
	public static void putProcess(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();
		int userId = (int) session.getAttribute("userId");
		String role = userServices.getUserRole(userId);
		
		if (session.getAttribute("userId") == null || !req.getMethod().equals("PUT") || !role.equals("Admin")) {
			
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;			
		}			

		switch(req.getRequestURI()) {
		
		case "/BankingAPI_V7/accounts":
			AccountController.updateAccount(req, resp);
			return;
			
		default: 
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}
		
		
	}
	

	
}
