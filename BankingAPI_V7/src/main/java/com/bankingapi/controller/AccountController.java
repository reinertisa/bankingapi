package com.bankingapi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.model.Account;
import com.bankingapi.model.AccountStatus;
import com.bankingapi.model.User;
import com.bankingapi.services.AccountServices;
import com.bankingapi.services.AccountServicesImpl;
import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.services.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AccountController {
	
	private static AccountServices accountServices = new AccountServicesImpl();
	private static UserServices userServices = new UserServicesImpl();
	
	public static void getAllAccounts(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");		
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find All Accounts - Account Controller process started");
		
		if(role.equals("Admin") || role.equals("Employee")) {
			List<Account> accounts = accountServices.getAllAccounts();
			
			if(accounts.size() != 0) {
				
				int count = accounts.size();
				
				Message message = userServices.getMessage(count + " account(s) " + "found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				
				List<PrivateAccount> privateAccounts = new ArrayList<>();
				for(Account account : accounts) {
					
					PrivateAccount privateAccount = 
							new PrivateAccount(account.getUserId(),
								account.getAccountId(), account.getBalance(), 
								account.getStatus().getStatus(), account.getType().getType());
					privateAccounts.add(privateAccount);
				}
				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccounts));
			} else {
				Message message = userServices.getMessage("No account in the bank system yet");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}					
		} else {
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
	
	
	public static void getAccountById(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find Accounts By Id - Account Controller process started");
		
		IdNode idNode = null;
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			idNode = mapper.readValue(req.getInputStream(), IdNode.class);
		} catch (Exception e) {
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			
			System.out.println("Account class not created");
			return;
		}
		
		int searchId = idNode.getId();
		
		if(role.equals("Admin") || role.equals("Employee") || 
					(role.equals("Standard") && 
					accountServices.getUserIdInAccountByAccountId(searchId)==userId)) {
		
			if(accountServices.hasAccount(searchId)) {
				
				Message message = userServices.getMessage("Account found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					
				Account result = accountServices.getAccountByAccountId(searchId);
					
				PrivateAccount privateAccount = 
						new PrivateAccount(result.getUserId(),
										   result.getAccountId(), 
										   result.getBalance(), 
										   result.getStatus().getStatus(), 											   result.getType().getType());
						
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccount));
				System.out.println("Account found successfully");
			} else {
				Message message = userServices.getMessage("Invalid fields");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));					
	
				message = userServices.getMessage("Account not found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				System.out.println("Account not found in the system");
			}
			
		} else {
				resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				Message message = userServices.getMessage("The requested action is not permitted");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));			
		}
	}	
	
	public static void getAccountByStatus(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find Accounts By Status - Account Controller process started");
		
		if(!(role.equals("Admin") || role.equals("Employee"))) {
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		AccountStatus searchStatus = null;		
		try {
			ObjectMapper mapper = new ObjectMapper();
			searchStatus = mapper.readValue(req.getInputStream(), AccountStatus.class);
		} catch (Exception e) {
			System.out.println("Class not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			return;
		}
		
		int searchStatusId = searchStatus.getStatusId();		
		
		List<Account> accounts = accountServices.getAllAccountsByStatus(searchStatusId);
		
		if(accounts.size() != 0) {		
			int count = accounts.size();
			String status = accounts.get(0).getStatus().getStatus();
			Message message = userServices.getMessage(count + " " + status + " account(s) " + "found in the system");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			List<PrivateAccount> privateAccounts = new ArrayList<>();
			for(Account account : accounts) {
				
				PrivateAccount privateAccount = 
						new PrivateAccount(
							account.getUserId(),
							account.getAccountId(), account.getBalance(), 
							account.getStatus().getStatus(), account.getType().getType());
				privateAccounts.add(privateAccount);
			}
			
			resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccounts));
			
			System.out.println("Account found successfully");
			
		} else {
			Message message = userServices.getMessage("There is no account with this status");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
		}	
	}
	
	public static void getAccountByOwner(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find Accounts By User - Account Controller process started");
		
		User user = null;		
		try {
			ObjectMapper mapper = new ObjectMapper();
			user = mapper.readValue(req.getInputStream(), User.class);
			
		} catch (Exception e) {
			System.out.println("Class not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			return;
		}
		
		int searchUserId = user.getUserId();
			
		if(role.equals("Admin") || role.equals("Employee") || (role.equals("Standard") && userId == searchUserId)) {
			
			List<Account> accounts = accountServices.getAccountsByUserId(searchUserId);
			
			if(accounts.size() != 0) {
				
				int count = accounts.size();
				
				Message message = userServices.getMessage(count + " account(s) " + "found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
											
				List<PrivateAccount> privateAccounts = new ArrayList<>();
				for(Account account : accounts) {
					
					PrivateAccount privateAccount = 
							new PrivateAccount(
								account.getUserId(),
								account.getAccountId(), account.getBalance(), 
								account.getStatus().getStatus(), account.getType().getType());
					privateAccounts.add(privateAccount);
				}
				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccounts));
				
				System.out.println("Account found successfully");			
				
			} else {
				Message message = userServices.getMessage("There is no account with this userid");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}
					
		} else {
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}
	}
	
	public static void getAcccountByUserIdAndType(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find Accounts By User and Account Type - Account Controller process started");
		
		User user = null;		
		try {
			ObjectMapper mapper = new ObjectMapper();
			user = mapper.readValue(req.getInputStream(), User.class);
			
		} catch (Exception e) {
			System.out.println("Class not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			return;
		}
		
		String accountType = req.getParameter("accountType");
		
		int searchUserId = user.getUserId();
			
		if(role.equals("Admin") || role.equals("Employee") || (role.equals("Standard") && userId == searchUserId)) {
			
			List<Account> accounts = accountServices.getAccountsByUserIdAndAccountType(searchUserId, accountType);
			
			if(accounts.size() != 0) {
				
				int count = accounts.size();
				
				Message message = userServices.getMessage(count + " " + accountType + " account(s) " + "found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
											
				List<PrivateAccount> privateAccounts = new ArrayList<>();
				for(Account account : accounts) {
					
					PrivateAccount privateAccount = 
							new PrivateAccount(
								account.getUserId(),
								account.getAccountId(), account.getBalance(), 
								account.getStatus().getStatus(), account.getType().getType());
					privateAccounts.add(privateAccount);
				}
				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccounts));
				
				System.out.println("Account found successfully");			
				
			} else {
				Message message = userServices.getMessage("There is no " + accountType + " account");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}
					
		} else {
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}	
		
	}
	
	public static void submitAccount(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);
		
		System.out.println("Submit Account - Account Controller process started");
		
		Account account = null;		
		try {
			ObjectMapper mapper = new ObjectMapper();
			account = mapper.readValue(req.getInputStream(), Account.class);
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		try {			
			
			if(role.equals("Admin") || role.equals("Employee") || 
					(role.equals("Standard") && userId == account.getUserId())) {
				
				String output = accountServices.isAccountCreated(account.getUserId(), account);

				if(output.equals("success")) {
					
					int lastAccountId = accountServices.getLastAccountId(account.getUserId());
					Account newAccount = accountServices.getAccountByAccountId(lastAccountId);

					Message message = userServices.getMessage("Account created successfully");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					
					PrivateAccount privateAccount = 
							new PrivateAccount(newAccount.getUserId(), newAccount.getAccountId(), 
											   newAccount.getBalance(), 
											   newAccount.getStatus().getStatus(), 
											   newAccount.getType().getType());
					
					resp.setStatus(HttpServletResponse.SC_CREATED);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccount));
					
				} else {
					
					Message message = userServices.getMessage(output);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					System.out.println(output);
				}
				
			} else {
				resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				Message message = userServices.getMessage("The requested action is not permitted");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));				
			}				
			
		}catch(Exception e) {			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));			
			System.out.println("Invalid field");
		}
		
	}
	
	public static void updateAccount(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		
		
		System.out.println("Update Account - Account Controller process started");
		
		Account willUpdateAccount = null;		
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			willUpdateAccount = mapper.readValue(req.getInputStream(), Account.class);
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		try {			
			
			Account account = accountServices.getAccountByAccountId(willUpdateAccount.getAccountId());
			
			if(account == null) {
				Message message = userServices.getMessage("Account not found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				System.out.println("Updated account not found");
				
			} else {
											
				String output = accountServices.isAccountUpdated(willUpdateAccount.getUserId(), willUpdateAccount);
				
				if(output.equals("success")) {			
					
					Account updatedAccount = accountServices.getAccountByAccountId(willUpdateAccount.getAccountId());
					
					Message message = userServices.getMessage("Account updated successfully");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					
					PrivateAccount privateAccount = 
							new PrivateAccount(updatedAccount.getUserId(),
									updatedAccount.getAccountId(), updatedAccount.getBalance(), 
									updatedAccount.getStatus().getStatus(), 
									updatedAccount.getType().getType());
					resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccount));
					
					System.out.println("Account updated successfully");
						
				} else {					
					Message message = userServices.getMessage(output);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				}					
					
			}
			
		}catch(Exception e) {
			System.out.println("Invalid field");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
	
	
	public static void passTime(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("PassTime - Account Controller process started");
		
		if(!role.equals("Admin")){
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		SavingsInterest accruedInterest = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			accruedInterest = mapper.readValue(req.getInputStream(), SavingsInterest.class);
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		
		int numOfMonths = accruedInterest.getNumOfMonths();
		double interestRate = accruedInterest.getInterestRate();
		
		
		int numOfAccruedInterestSavings = accountServices.numOfSavingsAccountbyaccruedInterest(numOfMonths);
		
		
		if(numOfAccruedInterestSavings > 0) {
		
			boolean interestApplied = accountServices.applyInterestforSavingsAccount(numOfMonths, interestRate);
			
			String response = "{" + accruedInterest.getNumOfMonths() + "} " + 
					  "months of interest has been accrued for " + 
					  "{" + numOfAccruedInterestSavings + "} Savings Accounts";
	
			Message message = userServices.getMessage(response);
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			List<Account> accounts = accountServices.getAccruedSavingsAccounts(numOfMonths);
			
			List<PrivateAccount> privateAccounts = new ArrayList<>();
			for(Account account : accounts) {
				
				PrivateAccount privateAccount = 
						new PrivateAccount(
							account.getUserId(),
							account.getAccountId(), account.getBalance(), 
							account.getStatus().getStatus(), account.getType().getType());
				privateAccounts.add(privateAccount);
			}
			
			
			
			resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccounts));			
		
		} else {
			
			String response = "Any savings account have not passed " + accruedInterest.getNumOfMonths() + " months yet";
	
			Message message = userServices.getMessage(response);
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}
		
	}
}

class PrivateAccount{
	int userId;
	int accountId;
	double balance;
	String status;
	String type;
	public PrivateAccount() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	public PrivateAccount(int userId, int accountId, double balance, String status, String type) {
		super();
		this.userId = userId;
		this.accountId = accountId;
		this.balance = balance;
		this.status = status;
		this.type = type;
	}



	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}



	@Override
	public String toString() {
		return "PrivateAccount [userId=" + userId + ", accountId=" + accountId + ", balance=" + balance + ", status="
				+ status + ", type=" + type + "]";
	}

	
	
	
}

class SavingsInterest{
	int numOfMonths;
	double interestRate;
	public SavingsInterest() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SavingsInterest(int numOfMonths, double interestRate) {
		super();
		this.numOfMonths = numOfMonths;
		this.interestRate = interestRate;
	}
	public int getNumOfMonths() {
		return numOfMonths;
	}
	public void setNumOfMonths(int numOfMonths) {
		this.numOfMonths = numOfMonths;
	}
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	@Override
	public String toString() {
		return "SavingsInterest [numOfMonths=" + numOfMonths + ", interestRate=" + interestRate + "]";
	}
}


class IdNode{
	int id;

	public IdNode() {
		// TODO Auto-generated constructor stub
	}
	public IdNode(int id) {
		super();
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "IdNode [id=" + id + "]";
	}
	
}
