package com.bankingapi.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpRetryException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.dao.UserDao;
import com.bankingapi.model.Role;
import com.bankingapi.model.User;
import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.servlethelper.UserRequestHelper;
import com.bankingapi.services.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserController {

	private static UserServices userServices = new UserServicesImpl();
	
	public static void getUser(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();
		int userId = (int) session.getAttribute("userId");
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);
		
		System.out.println("Find Users By Id User controller started");

		IdNode idNode = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			idNode = mapper.readValue(req.getInputStream(), IdNode.class);			

		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		int id = idNode.getId();			
		User user = userServices.getUser(id);
		
		if(user != null) {
			
			PrivateUser privateUser = new PrivateUser(user.getUserId(), user.getUsername(), 
													  "**********",user.getFirstName(), user.getLastName(), 
		                                              user.getEmail(), user.getRole().getRole());
			
			if (role.equals("Admin") || role.equals("Employee")) {	
			
				Message message = userServices.getMessage("User found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateUser));
			
			} else if (role.equals("Standard") && id == userId) {				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateUser));
				
			} else {		
				resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				Message message = userServices.getMessage("The requested action is not permitted");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}			
		} else {			
			Message message = userServices.getMessage("User not found in the system");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}			
	}		
	
	
	public static void getAllUser(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
			
		HttpSession session = req.getSession();
		int userId = (int) session.getAttribute("userId");
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);		
		
		System.out.println("Find All Users - User controller started");
		
		if(role.equals("Admin") || role.equals("Employee")) {	
			
			List<User> users = userServices.getAllUsers();
			int count = userServices.numOfUser();
			
			Message message = userServices.getMessage(count + " user(s) " + "found in the system");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			if(users != null) {		
				List<PrivateUser> privateUsers = new ArrayList<>();
				for(User user : users) {
					PrivateUser privateUser = new PrivateUser(user.getUserId(), user.getUsername(), 
													          "**********",user.getFirstName(), user.getLastName(), 
															  user.getEmail(), user.getRole().getRole());
					privateUsers.add(privateUser);					
				}
				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateUsers));

			} else {				
				message = userServices.getMessage("User not found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}	
			
		} else {	
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}
		
	} 
	
	public static void addUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		int userId = 0;
		try {
			HttpSession session = req.getSession();
			userId = (int) session.getAttribute("userId");			
		} catch (Exception e) {
			System.out.println("There is no session yet");
		}
		
		resp.setContentType("application/json");
			
		
		System.out.println("Register/Add user - User controller started");
		
		User newUser = null;
		try {
			ObjectMapper mapper  = new ObjectMapper();		
			newUser = mapper.readValue(req.getInputStream(), User.class); //user with zero id
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			message = userServices.getMessage("All fields are empty, please fill them out");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		try {			
			String role = userServices.getUserRole(userId);
			
			if(role == null) {
				
				if(newUser.getRole().getRole().equals("Admin")){
					
					if(userServices.getNumOfAdmin() == 0) {
						
						String output = userServices.registerUser(newUser);
						if (output.equals("success")) {			
							String responseMessage = "Hi, {" + newUser.getUsername() + "}, " +
													 "you registered as ADMIN the online bank system successfully";
						
							Message message = userServices.getMessage(responseMessage);
							resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					
							resp.setStatus(HttpServletResponse.SC_CREATED);
							User user = userServices.getUser(newUser.getUsername(), newUser.getPassword()); //user with new id
							PrivateUser privateUser = new PrivateUser(user.getUserId(), user.getUsername(), 
																  "**********",user.getFirstName(), user.getLastName(), 
								  								  user.getEmail(), user.getRole().getRole());
						
							resp.getWriter().write(new ObjectMapper().writeValueAsString(privateUser));
						
							System.out.println("New admin created");
							return;
						
						} else {
							Message message = userServices.getMessage(output);
							resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
							resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);						
							return;
						}
						
					} else {
					
						Message message = userServices.getMessage("Sorry, no more than one Admin. It is not allowed");
						resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
						resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);						
						return;
					}
				} else if(newUser.getRole().getRole().equals("Standard")) {
					
					String output = userServices.registerUser(newUser);
					if (output.equals("success")) {			
						String responseMessage = "Hi, {" + newUser.getUsername() + "}, " +
												 "you registered the online bank system successfully";
					
						Message message = userServices.getMessage(responseMessage);
						resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				
						resp.setStatus(HttpServletResponse.SC_CREATED);
						User user = userServices.getUser(newUser.getUsername(), newUser.getPassword()); //user with new id
						PrivateUser privateUser = new PrivateUser(user.getUserId(), user.getUsername(), 
															  "**********",user.getFirstName(), user.getLastName(), 
							  								  user.getEmail(), user.getRole().getRole());
					
						resp.getWriter().write(new ObjectMapper().writeValueAsString(privateUser));
						
					} else {
						Message message = userServices.getMessage(output);
						resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
						resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					}
					
				} else {
					Message message = userServices.getMessage("The requested action is not permitted");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					message = userServices.getMessage("Admin can register new user(Employee or Standard) or Standard users should be able to register");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				}
			
			} else {
				
				if(role.equals("Admin")) {
					String output = userServices.registerUser(newUser);
					if (output.equals("success")) {			
						String responseMessage = "Hi, {" + newUser.getUsername() + "}, " +
												 "you registered the online bank system successfully";
					
						Message message = userServices.getMessage(responseMessage);
						resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				
						resp.setStatus(HttpServletResponse.SC_CREATED);
						User user = userServices.getUser(newUser.getUsername(), newUser.getPassword()); //user with new id
						PrivateUser privateUser = new PrivateUser(user.getUserId(), user.getUsername(), 
															  "**********",user.getFirstName(), user.getLastName(), 
							  								  user.getEmail(), user.getRole().getRole());
					
						resp.getWriter().write(new ObjectMapper().writeValueAsString(privateUser));
						
					} else {
						Message message = userServices.getMessage(output);
						resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
						resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					}
				} else {
					Message message = userServices.getMessage("The requested action is not permitted");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					message = userServices.getMessage("Admin can register new user(Employee or Standard) or Standard users should be able to register");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				}
			}
			
			
		} catch (Exception e) {
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			message = userServices.getMessage("Admin can register new user(Employee or Standard) or Standard users should be able to register");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
	
	public static void updateUser(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
				
		HttpSession session = req.getSession();	
		resp.setContentType("application/json");
		int userId = (int)session.getAttribute("userId");
		String role = userServices.getUserRole(userId);
		
		System.out.println("Update user - User controller started");
		
		User willUpdateuser = null;
		try {
			ObjectMapper mapper  = new ObjectMapper();		
			willUpdateuser = mapper.readValue(req.getInputStream(), User.class);	
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		if(role.equals("Admin") || userId == willUpdateuser.getUserId()) {		
			
			String output = userServices.updateUser(willUpdateuser, userId);
			
			if (output.equals("success")) {				
				User updatedUser = userServices.getUser(willUpdateuser.getUserId()); // updated user;
				
				String responseMessage = "Information updated successfully";

				Message message = userServices.getMessage(responseMessage);
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
											
				PrivateUser privateUser = new PrivateUser(updatedUser.getUserId(), updatedUser.getUsername(), 
														  "**********", updatedUser.getFirstName(), updatedUser.getLastName(), 
														  updatedUser.getEmail(), updatedUser.getRole().getRole());
				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateUser));
				
				System.out.println("User updated");
				
			} else {
				Message message = userServices.getMessage(output);
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));			
			}
			
		} else {
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
}


class PrivateUser{
	
	private int userId; 
	private String username; 
	private String password;
	private String firstName; 
	private String lastName; 
	private String email; 
	private String role;
	public PrivateUser() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PrivateUser(int userId, String username, String password, String firstName, String lastName, String email,
			String role) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.role = role;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "PrivateUser [userId=" + userId + ", username=" + username + ", password=" + password + ", firstName="
				+ firstName + ", lastName=" + lastName + ", email=" + email + ", role=" + role + "]";
	}
}

class IdNode{
	int id;

	public IdNode() {
		// TODO Auto-generated constructor stub
	}
	public IdNode(int id) {
		super();
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "IdNode [id=" + id + "]";
	}
	
}


