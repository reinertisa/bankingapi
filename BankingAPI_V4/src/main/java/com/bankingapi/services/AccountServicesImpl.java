package com.bankingapi.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.util.List;

import com.bankingapi.dao.AccountDao;
import com.bankingapi.dao.AccountDaoImpl;
import com.bankingapi.dao.UserDao;
import com.bankingapi.model.Account;
import com.bankingapi.model.AccountStatus;
import com.bankingapi.model.AccountType;


public class AccountServicesImpl implements AccountServices{

	private AccountDao accountDao = new AccountDaoImpl();
	private UserServices userServices = new UserServicesImpl();	

	@Override
	public String isAccountCreated(int userId, Account account) {
		
		if(!userServices.hasUser(userId)) {
			return "User not found in the system";
		}
						
		if(account.getBalance() < 0) {
			return "Invalid balance(balance >=0)";	
		}
		
		if(!accountDao.isValidStatusId(account.getStatus().getStatusId())) {
			return "Invalid status id";
		}
		
		if(!accountDao.isValidStatus(account.getStatus().getStatus())) {
			return "Invalid status";
		}
		
		if(!accountDao.isMatchStatusIdAndStatus(account.getStatus().getStatusId(), account.getStatus().getStatus())) {
			return "Status id and status not match";
		}
		
		if(!accountDao.isValidTypeId(account.getType().getTypeId())) {
			return "Invalid type id";
		}		
		
		if(!accountDao.isValidType(account.getType().getType())) {
			return "Invalid type";
		}

		if(!accountDao.isMatchTypeIdAndType(account.getType().getTypeId(), account.getType().getType())) {
			return "Type id and type not match";
		}
		
		if(accountDao.isAccountCreated(userId, account)) {
			return "success";
		} else {
			return "fail";
		}
	}	
	
	@Override
	public String isAccountUpdated(int userId, Account account) {
		
		if(!userServices.hasUser(userId)) {
			return "User not found in the system";
		}
		
		if(account.getBalance() < 0) {
			return "Invalid balance(balance >=0)";	
		}
		
		if(!accountDao.isValidStatusId(account.getStatus().getStatusId())) {
			return "Invalid status id";
		}
		
		if(!accountDao.isValidStatus(account.getStatus().getStatus())) {
			return "Invalid status";
		}
		
		if(!accountDao.isMatchStatusIdAndStatus(account.getStatus().getStatusId(), account.getStatus().getStatus())) {
			return "Status id and status not match";
		}
		
		if(!accountDao.isValidTypeId(account.getType().getTypeId())) {
			return "Invalid type id";
		}		
		
		if(!accountDao.isValidType(account.getType().getType())) {
			return "Invalid type";
		}

		if(!accountDao.isMatchTypeIdAndType(account.getType().getTypeId(), account.getType().getType())) {
			return "Type id and type not match";
		}
		
		if(accountDao.isAccountUpdated(userId, account)) {
			return "success";
		} else {
			return "fail";
		}
	}
	
	@Override
	public List<Account> getAllAccounts() {
		
		return accountDao.getAllAccounts();
	}

	@Override
	public boolean hasEnoughBalanceForWithdrawal(int accountId, double amount) {
		
		return accountDao.getBalanceByAccountId(accountId) >= amount;
	}

	@Override
	public boolean hasEnoughOpenAccounts(int userId) {
		
		return accountDao.getOpenAccountId(userId) > 0;		
	}
	
	public boolean hasEnoughOpenAccountsForTransferMoneyInYourAccounts(int userId) {
		
		return accountDao.getOpenAccountId(userId) > 1;		
	}

	@Override
	public boolean isEqualAccountId(int firstAccount, int secondAccount) {
		return accountDao.isEqualAccountId(firstAccount, secondAccount);
	}


	@Override
	public boolean isOpenAccount(int accountId) {
		return accountDao.getAccountStatus(accountId).equals("Open");
	}


	@Override
	public boolean isPendingAccount(int accountId) {
		return accountDao.getAccountStatus(accountId).equals("Pending");
	}

	
	@Override
	public List<Account> getAccountsByUserId(int userId) {
		
		return accountDao.getAllAccountsByUserId(userId);

	}

	@Override
	public int getAccountIdByUserId(int userId) {
		return accountDao.getAccountIdByUserId(userId);
	}

	@Override
	public boolean isOpenAccount(int accountId, int userId) {
		return accountDao.getAccountStatusByAccountId(accountId, userId).equals("Open");
	}

	@Override
	public boolean isAccountOwnedByUser(int accountId, int userId) {
		return accountDao.isAccountOwnedByUser(accountId, userId);
	}

	@Override
	public List<Account> getAllAccountsByStatus(int statusId) {
		return accountDao.getAllAccountsByStatusId(statusId);
	}

	@Override
	public Account getAccountByAccountId(int accountId) {
		return accountDao.getAccountByAccountId(accountId);
	}

	@Override
	public boolean hasAccount(int accountId) {
		return accountDao.hasAccountIdbyAccountId(accountId);
	}


	@Override
	public int getLastAccountId(int userId) {
		return accountDao.getLastAccountId(userId);
	}


	@Override
	public String isTransferFunds(int sourceAccountId, int targetAccountId, double amount) {
		
		
		if(isEqualAccountId(sourceAccountId, targetAccountId)) {
			return "Source accountId and target accountId are the same";
		}
	
		String withdrawalResponse = isMakeAWithdrawal(sourceAccountId, amount);		
		
		if(withdrawalResponse.equals("Invalid account")) {
			return "Source account id is invalid";
		} else if(withdrawalResponse.equals("Not an open account")) {
			return "Source account is not an open account";
		} else if(withdrawalResponse.equals("Invalid amount")) {
			return "Invalid amount";
		} else if(withdrawalResponse.equals("Not enough balance in your account")) {
			return "Not enough balance in your account";
		}
		
			
		String depositResponse = isMakeADeposit(targetAccountId, amount);
	
		if(depositResponse.equals("Invalid account")) {
			return "Target account id is invalid";
		} else if(depositResponse.equals("Not an open account")) {
			return "Target account is not an open account";
		}
		
		return "success";
	}
	
	public boolean makeTransferFunds(int sourceAccountId, int targetAccountId, double amount) {
		
		return makeAWithdrawal(sourceAccountId, amount) &&
			   makeADeposit(targetAccountId, amount);
	}


	@Override
	public String isMakeAWithdrawal(int accountId, double amount) {
		
		if(!hasAccount(accountId)) {
			return "Invalid account";
		}
		
		if(!isOpenAccount(accountId)) {
			return "Not an open account";
		}
		
		if(amount <= 0) {
			return "Invalid amount";
		}	

		
		if(!hasEnoughBalanceForWithdrawal(accountId, amount)) {
			return "Not enough balance in your account";
		}
		
		return "success";
				
	}
	
	public boolean makeAWithdrawal(int accountId, double amount) {
		return accountDao.makeAWithdrawal(accountId, amount);
	}

	@Override
	public String isMakeADeposit(int accountId, double amount) {
		
		if(!hasAccount(accountId)) {
			return "Invalid account";
		}
		
		if(!isOpenAccount(accountId)) {
			return "Not an open account";
		}
		
		if(amount <= 0) {
			return "Invalid amount";
		}	

		
		return "success";
	}
	
	public boolean makeADeposit(int accountId, double amount) {
		return accountDao.makeADeposit(accountId, amount);
	}


	@Override
	public int getUserIdInAccountByAccountId(int accountId) {
		
		return accountDao.getUserIdInAccountByAccountId(accountId);
	
	}


	@Override
	public boolean isValidStatusId(int statusId) {
		return accountDao.isValidStatusId(statusId);
	}


	@Override
	public boolean isValidStatus(String status) {
		return accountDao.isValidStatus(status);
	}


	@Override
	public boolean isMatchStatusIdAndStatus(int statusId, String status) {
		return accountDao.isMatchStatusIdAndStatus(statusId, status);
	}


	@Override
	public boolean isValidTypeId(int typeId) {
		return accountDao.isValidTypeId(typeId);
	}


	@Override
	public boolean isValidType(String type) {
		return accountDao.isValidType(type);
	}

	@Override
	public boolean isMatchTypeIdAndType(int typeId, String type) {
		return accountDao.isMatchTypeIdAndType(typeId, type);
	}

	@Override
	public int numofMonthsForSavings(int numOfMonths) {
		
		List<LocalDate> dates = accountDao.accruedInterestForSavings(numOfMonths);
		LocalDate now = LocalDate.now();
		
		int accruedInterest = 0;
	
		for(LocalDate date : dates) {
			
			Period diff = Period.between(date,now);
			
			if(diff.toTotalMonths() >= numOfMonths) {
				accruedInterest++;
			}
			
		}
			
		return accruedInterest;
	}


}


