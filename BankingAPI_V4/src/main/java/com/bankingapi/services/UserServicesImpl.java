package com.bankingapi.services;

import java.util.List;

import org.mindrot.jbcrypt.BCrypt;

import com.bankingapi.dao.UserDao;
import com.bankingapi.dao.UserDaoImpl;
import com.bankingapi.model.User;

public class UserServicesImpl implements UserServices{

	private UserDao userDao = new UserDaoImpl();

	
	@Override
	public String registerUser(User newUser) {
		
		String result= confirmUserInfo(newUser);
		
		if(!result.equals("confirmationSuccess")) {
			return result;
		}
		String tempPass = newUser.getPassword();
		String hashPass = hashPassword(newUser.getPassword());
		newUser.setPassword(hashPass);
		
		if(userDao.isCreateUser(newUser)) {
			newUser.setPassword(tempPass);
			return "success";
		} else {
			return "user not created";
		}
	}	
	
	public String hashPassword(String plainTextPassword){
		return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt(12));
	}
	
	public boolean checkPass(String plainPassword, String hashedPassword) {
		return BCrypt.checkpw(plainPassword, hashedPassword);
	}
	
	@Override
	public String getHashedPassword(String username) {

		return userDao.getHashedPassword(username);
	}

	
	
	@Override
	public User getUser(String username, String password) {
		
		String hashedPassword = getHashedPassword(username);
		
		if(BCrypt.checkpw(password, hashedPassword)) {
			int userId = userDao.getUserId(username, hashedPassword);
			if(userId == -1) {
				return null;
			} else {
				return userDao.getUser(userId);	
			}
		}
		
		return null;
	}

	
	
	@Override
	public String updateUser(User willUpdateUser, int userId) {
		
		if(!userDao.hasUser(willUpdateUser.getUserId())) {
			return "User not found in the system";
		}
		
		if(!isValidUsername(willUpdateUser.getUsername())) {
			return "Invalid username";
		}
		
		if(!userDao.isAvailableUsername(willUpdateUser.getUsername())) {
			if(!userDao.isYourUsername(willUpdateUser.getUsername(), willUpdateUser.getUserId())) {
				return "Username not available";
			}
		}	
		
		if(!isValidPassword(willUpdateUser.getPassword())) {
			return "Invalid password";
		}
		
		if(!isValidFirstName(willUpdateUser.getFirstName())) {
			return "Invalid firstname";
		}
		
		if(!isValidLastName(willUpdateUser.getLastName())) {
			return "Invalid lastname";
		}
		
		if(!isValidEmail(willUpdateUser.getEmail())) {
			return "Invalid email";
		}
		
		if(!userDao.isAvailableEmail(willUpdateUser.getEmail())) {
			if(!userDao.isYourEmail(willUpdateUser.getEmail(), willUpdateUser.getUserId())) {
				return "Email not available";
			}
		}
		
		User user = userDao.getUser(userId);
		
		if(user.getRole().getRole().equals("Admin")) {
			
			if(willUpdateUser.getRole().getRoleId() == 1 || willUpdateUser.getRole().getRole().equals("Admin")) {
				return "Sorry, no more than one Admin. It is not allowed";
			}
		} else {
			
			if(user.getRole().getRoleId() != willUpdateUser.getRole().getRoleId()) {
				return "Sorry, only admin can update your role";
			}
			
			if(!user.getRole().getRole().equals(willUpdateUser.getRole().getRole())){
				return "Sorry, only admin can update your role";
			}
			
		}
		
		if(!isValidRoleId(willUpdateUser.getRole().getRoleId())){
			return "Invalid roleid";
			
		}
		if(!isMatchRoleIdAndRole(willUpdateUser.getRole().getRoleId(), willUpdateUser.getRole().getRole())) {
			return "Roleid and Role not matched";				
		}			
	
		//password hashing
		String hashPass = hashPassword(willUpdateUser.getPassword());
		willUpdateUser.setPassword(hashPass);
		
		
		if(user.getRole().getRole().equals("Admin")) {
			if(userDao.updateUserByAdmin(willUpdateUser)) {
				return "success";
			} else {
				return "fail";
			}
		} else {
			if(userDao.updateUser(willUpdateUser)) {
				return "success";
			} else {
				return "user not updated";
			}	
		}

	}
	
	public String confirmUserInfo(User user) {
		
		if(user.getRole().getRole().equals("Admin") && getNumOfAdmin() > 0) {
			return "Sorry, no more than one Admin. It is not allowed";
		}
		
		if(!isValidUsername(user.getUsername())) {
			return "Invalid username";
		}
		
		if(!isAvailableUsername(user.getUsername())) {
			return "Username not available";
		}
		
		if(!isValidPassword(user.getPassword())) {
			return "Invalid password";
		}
		
		if(!isValidFirstName(user.getFirstName())) {
			return "Invalid firstname";
		}
		
		if(!isValidLastName(user.getLastName())) {
			return "Invalid lastname";
		}
		
		if(!isValidEmail(user.getEmail())) {
			return "Invalid email";
		}
		
		if(!isAvailableEmail(user.getEmail())) {
			return "Email not available";
		}			
		
		if(!isValidRoleId(user.getRole().getRoleId())){
			return "Invalid roleid";
		}
		
		if(!isMatchRoleIdAndRole(user.getRole().getRoleId(), user.getRole().getRole())) {
			return "Roleid and Role not matched";				
		}
		
		return "confirmationSuccess";
		
	}
	
	@Override
	public User getUser(int userId) {
		
		if(userDao.hasUser(userId)) {
			return userDao.getUser(userId);
		}		
		return null;		
	}



	@Override
	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}

//	@Override
//	public User getUser(String searchId) {
//		
//		try {
//			return userDao.getUserByUserId(Integer.parseInt(searchId));
//		} catch (Exception e) {
//			System.out.println("Exception: searchid not found in getUser");
//		}
//	
//		return null;	
//	}

	@Override
	public Message getMessage(String message) {		
		return new Message(message);
	}
	
	@Override
	public String getUserRole(int userId) {		
		return userDao.getRolebyUserId(userId);
	}

	@Override
	public boolean hasUser(int userId) {

		return userDao.hasUser(userId);
	}

	@Override
	public String getUsername(int userId) {
		return userDao.getUsername(userId);
	}
	
	@Override
	public boolean isAvailableUsername(String username) {
		
		return !userDao.isAvailableUsername(username) ? false : true;

	}
	
	@Override
	public boolean isAvailableEmail(String email) {
		
		return !userDao.isAvailableEmail(email) ? false : true;
	}
	
	@Override
	public boolean isValidUsername(String username) {
		
		return (username == null || username.trim().length() == 0) ? false : true;
	}
	
	@Override
	public boolean isValidPassword(String password) {
		
		return (password == null || password.trim().length() == 0) ? false : true;	
	}
	
	@Override
	public boolean isValidFirstName(String firstName) {
		
		return (firstName == null || firstName.trim().length() == 0) ? false : true;
	}
	
	@Override
	public boolean isValidLastName(String lastName) {
		
		return (lastName == null || lastName.trim().length() == 0) ? false : true;
	
	}
	
	@Override
	public boolean isValidEmail(String email) {
		
		return (email == null || email.trim().length() == 0) ? false : true;		
	}
	
	@Override
	public boolean isValidRoleId(int roleId) {
	
		return userDao.isValidRoleId(roleId);
	}

	@Override
	public boolean isMatchRoleIdAndRole(int roleId, String role) {
		return userDao.isMatchRoleIdAndRole(roleId, role);
		
	}
		
	@Override
	public int numOfUser() {
		return userDao.numOfUser();		
	}

	
	@Override
	public int getNumOfAdmin() {
		return userDao.getNumOfAdmin();
	}

 }


 