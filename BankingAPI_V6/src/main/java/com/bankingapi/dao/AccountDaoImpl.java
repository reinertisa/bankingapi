package com.bankingapi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.bankingapi.model.Account;
import com.bankingapi.model.AccountStatus;
import com.bankingapi.model.AccountType;
import com.bankingapi.utilities.DaoConnection;

public class AccountDaoImpl implements AccountDao {

	Connection conn = null;
	PreparedStatement pst = null;
	
	@Override
	public boolean isAccountCreated(int userId, Account account) {
	
		try {
			String sql = "INSERT INTO account(balance, userid, statusid, typeid, createdate) VALUES(?,?,?,?,?)";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setDouble(1, account.getBalance());
			pst.setInt(2, account.getUserId());
			pst.setInt(3, account.getStatus().getStatusId());
			pst.setInt(4, account.getType().getTypeId());
			//pst.setDate(5,Date.valueOf(LocalDate.now()));
			pst.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()));
		
			//two different way to set date
			
			pst.executeUpdate();
			return true;
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return false;
	}

	@Override
	public List<Account> getAllAccounts() {
		
		List<Account> accounts = new ArrayList<>();
		try {
			String sql = "SELECT a.userid, a.accountid, round(a.balance :: NUMERIC, 2) AS balance, \r\n" + 
					"b.status , b.statusid ,c.type, c.typeid FROM account a \r\n" + 
					"INNER JOIN accountstatus b ON a.statusid = b.statusid \r\n" + 
					"INNER JOIN accounttype c ON a.typeid = c.typeid ORDER BY a.accountid;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			ResultSet rs = pst.executeQuery();			
		
			
			while(rs.next()) {
				Account account = new Account();
				account.setUserId(rs.getInt("userid"));
				account.setAccountId(rs.getInt("accountid"));
				account.setBalance(rs.getDouble("balance"));
				account.setStatus(new AccountStatus(rs.getInt("statusid"), rs.getString("status")));
				account.setType(new AccountType(rs.getInt("typeId"), rs.getString("type")));				
				accounts.add(account);
			}
						
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return accounts;
	}

	@Override
	public Account getAccountByAccountId(int accountId) {
		
		Account account = new Account();
		
		try {
			String sql = "SELECT a.userid, a.accountid, TRUNC(a.balance :: NUMERIC, 2) AS balance, \n" + 
					"b.statusid , b.status , c.typeid, c.TYPE, a.createdate FROM account a \n" + 
					"INNER JOIN accountstatus b ON a.statusid = b.statusid \n" + 
					"INNER JOIN accounttype c ON a.typeid = c.typeid \n" + 
					"WHERE a.accountid = ?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, accountId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				account.setUserId(rs.getInt("userid"));
				account.setAccountId(rs.getInt("accountid"));
				account.setBalance(rs.getDouble("balance"));
				account.setStatus(new AccountStatus(rs.getInt("statusid"), rs.getString("status")));
				account.setType(new AccountType(rs.getInt("typeId"), rs.getString("type")));
				account.setCreateDate(rs.getDate("createdate").toLocalDate());
				return account;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return null;
	}
	
	@Override
	public List<Account> getAllAccountsByStatusId(int statusId) {
	
		List<Account> accounts = new ArrayList<>();
		try {
			String sql = "SELECT a.userid, a.accountid, trunc(balance :: NUMERIC, 2) AS balance, \r\n" + 
					"a.statusid, a.typeid ,b.status ,c.type FROM account a \r\n" + 
					"INNER JOIN accountstatus b ON a.statusid  = b.statusid \r\n" + 
					"INNER JOIN accounttype c ON a.typeid = c.typeid WHERE a.statusid = ?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, statusId);
			ResultSet rs = pst.executeQuery();			
		
		
			while(rs.next()) {
				Account account = new Account();
				account.setUserId(rs.getInt("userid"));
				account.setAccountId(rs.getInt("accountid"));
				account.setBalance(rs.getDouble("balance"));
				account.setStatus(new AccountStatus(rs.getInt("statusid"), rs.getString("status")));
				account.setType(new AccountType(rs.getInt("typeId"), rs.getString("type")));				
				accounts.add(account);
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return accounts;
	}
	
	@Override
	public List<Account> getAllAccountsByUserId(int userId) {
		List<Account> accounts = new ArrayList<>();
		
		try {
			String sql = "SELECT a.userid, a.accountid , trunc(a.balance :: NUMERIC, 2) AS balance, \r\n" + 
					"a.statusid, a.typeid, b.status , c.type FROM account a \r\n" + 
					"INNER JOIN accountstatus b ON a.statusid = b.statusid \r\n" + 
					"INNER JOIN accounttype c ON a.typeid = c.typeid \r\n" + 
					"WHERE userid=? ORDER BY a.accountid DESC;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			ResultSet rs = pst.executeQuery();			
		
			while(rs.next()) {
				Account account = new Account();
				account.setUserId(rs.getInt("userid"));
				account.setAccountId(rs.getInt("accountid"));
				account.setBalance(rs.getDouble("balance"));
				account.setStatus(new AccountStatus(rs.getInt("statusid"), rs.getString("status")));
				account.setType(new AccountType(rs.getInt("typeId"), rs.getString("type")));				
				accounts.add(account);
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return accounts;
	}
	
	@Override
	public List<Account> getAccountsByUserIdAndAccountType(int userId, String accountType) {
		List<Account> accounts = new ArrayList<>();
		
		try {
			String sql = "SELECT a.userid, a.accountid , trunc(a.balance :: NUMERIC, 2) AS balance, \n" + 
					"a.statusid, a.typeid, b.status , c.type FROM account a \n" + 
					"INNER JOIN accountstatus b ON a.statusid = b.statusid \n" + 
					"INNER JOIN accounttype c ON a.typeid = c.typeid \n" + 
					"WHERE userid=? AND c.TYPE=? ORDER BY a.accountid DESC;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			pst.setString(2, accountType);
			ResultSet rs = pst.executeQuery();			
		
			while(rs.next()) {
				Account account = new Account();
				account.setUserId(rs.getInt("userid"));
				account.setAccountId(rs.getInt("accountid"));
				account.setBalance(rs.getDouble("balance"));
				account.setStatus(new AccountStatus(rs.getInt("statusid"), rs.getString("status")));
				account.setType(new AccountType(rs.getInt("typeId"), rs.getString("type")));				
				accounts.add(account);
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return accounts;
	}
	
	

	@Override
	public List<Account> getAllAccountsByType(int typeId) {
		List<Account> accounts = new ArrayList<>();
		
		try {
			String sql = "SELECT * FROM account a INNER JOIN accounttype b ON a.typeid = b.typeid \r\n" + 
					"FULL OUTER JOIN accountstatus c ON a.statusid = c.statusid "
					+ "WHERE b.typeid = ? ORDER BY a.accountid DESC;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, typeId);
			ResultSet rs = pst.executeQuery();			
		
			while(rs.next()) {
				Account account = new Account();
				account.setAccountId(rs.getInt("accountid"));
				account.setBalance(rs.getDouble("balance"));
				account.setStatus(new AccountStatus(rs.getInt("statusid"), rs.getString("status")));
				account.setType(new AccountType(rs.getInt("typeId"), rs.getString("type")));				
				accounts.add(account);
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return accounts;
	}





	@Override
	public List<Account> getAllAccountsByStatusId(int userId, int statusId) {
		List<Account> accounts = new ArrayList<>();
		
		try {
			String sql = "SELECT * FROM account a INNER JOIN accountstatus b "
					+ "ON a.statusid = b.statusid \r\n" + "FULL OUTER JOIN accounttype c "
					+ "ON a.typeid = c.typeid WHERE " + "a.userid = ? AND a.statusid = ?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			pst.setInt(2, statusId);
			ResultSet rs = pst.executeQuery();			
		
			while(rs.next()) {
				Account account = new Account();
				account.setAccountId(rs.getInt("accountid"));
				account.setBalance(rs.getDouble("balance"));
				account.setStatus(new AccountStatus(rs.getInt("statusid"), rs.getString("status")));
				account.setType(new AccountType(rs.getInt("typeId"), rs.getString("type")));				
				accounts.add(account);
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return accounts;
	}

	

	@Override
	public List<Account> getAllAccountsByRole(int roleId) {
		List<Account> accounts = new ArrayList<>();
		
		try {
			String sql = "SELECT * FROM account a FULL OUTER JOIN accountstatus b ON "
					+ "a.statusid = b.statusid \r\n" + "FULL OUTER JOIN accounttype c ON"
					+ " a.typeid = c.typeid \r\n" +	"WHERE "
					+ "a.userid IN (SELECT d.userid FROM users d WHERE d.roleid=?);";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, roleId);
			ResultSet rs = pst.executeQuery();			
		
			while(rs.next()) {
				Account account = new Account();
				account.setAccountId(rs.getInt("accountid"));
				account.setBalance(rs.getDouble("balance"));
				account.setStatus(new AccountStatus(rs.getInt("statusid"), rs.getString("status")));
				account.setType(new AccountType(rs.getInt("typeId"), rs.getString("type")));				
				accounts.add(account);
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return accounts;
	}
	
	@Override
	public double getBalanceByAccountId(int accountId) {
	
		double balance = 0.0D;
		
		try {
				
			String sql = "SELECT a.balance FROM account a WHERE a.accountid = ?";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, accountId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				balance = rs.getDouble("balance");
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return balance;
		
	}
	
	@Override
	public boolean makeAWithdrawal(int accountId, double amount) {
		
		try {
			String sql = "UPDATE account SET balance = trunc((balance - ?) :: NUMERIC, 2) WHERE accountid = ?;";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setDouble(1, amount);
			pst.setInt(2, accountId);
			
			pst.executeUpdate();
			
			return true;
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return false;
	}

	@Override
	public boolean makeADeposit(int accountId, double amount) {
		try {
			String sql = "UPDATE account SET balance = trunc((balance + ?) :: NUMERIC, 2) WHERE accountid = ?;";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setDouble(1, amount);
			pst.setInt(2, accountId);
			
			pst.executeUpdate();
			
			return true;
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		return false;		
	}

	@Override
	public boolean transferFunds(int fromAccountId, int toAccountId, double cash) {
		
		return this.makeAWithdrawal(fromAccountId, cash) & this.makeADeposit(toAccountId, cash);
		
	}


	@Override
	public void updateAccountTypeByAccountId(int accountId, int typeId) {
		try {
			String sql = "UPDATE account SET typeid=? WHERE accountid = ?;";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setInt(1, typeId);
			pst.setInt(2, accountId);
			
			pst.executeUpdate();
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
	}
		

	@Override
	public void updateAccountStatusByAccountId(int accountId, int statusId) {
		try {
			String sql = "UPDATE account SET statusid=? WHERE accountid = ?;";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setInt(1, statusId);
			pst.setInt(2, accountId);
			
			pst.executeUpdate();
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
	}
	
	@Override
	public boolean updateAccountStatusByOpen(int accountId) {
		try {
			String sql = "UPDATE account SET statusid=2 WHERE accountid = ?;";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setInt(1, accountId);			
			int count = pst.executeUpdate();
			
			if(count == 1) {
				return true;
			}
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		return false;
	}
	
	public boolean updateAccountStatusByDenied(int accountId) {
		try {
			String sql = "UPDATE account SET statusid=4 WHERE accountid = ?;";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setInt(1, accountId);
			int count = pst.executeUpdate();
			
			if(count == 1) {
				return true;
			}
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return false;
	}

	@Override
	public void deleteAccountByAccountId(int accountId) {
		try {
			String sql = "DELETE FROM account WHERE accountid = ?;";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setInt(1, accountId);
			
			pst.executeUpdate();
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
	}

	@Override
	public void deleteAccountByUserId(int userId) {
		try {
			String sql = "DELETE FROM account WHERE userid = ?;";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setInt(1, userId);
			
			pst.executeUpdate();
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		}
		
	}

	@Override
	public int getAccountIdByUserId(int userId) {
		
		try {
				
			String sql = "SELECT accountid FROM account WHERE userid = ?";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return rs.getInt("accountid");
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return -1;
	}

	
	
	@Override
	public boolean hasAccountIdbyAccountId(int accountId) {
		try {
			
			String sql = "SELECT accountid FROM account WHERE accountid = ?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, accountId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return false;
	}

	
	@Override
	public String getAccountStatusByAccountId(int accountId, int userId) {
		
		String str = "";
		
		try {
			
			String sql = "SELECT a.status FROM accountstatus a \r\n" + 
					"INNER JOIN account b ON a.statusid = b.statusid \r\n" + 
					"WHERE b.accountid = ? AND b.userid = ?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, accountId);
			pst.setInt(2, userId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return rs.getString("status");
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return str;
	}

	@Override
	public boolean isAccountOwnedByUser(int accountId, int userId) {
		try {
			
			String sql = "SELECT accountid FROM account WHERE accountid = ? AND userid = ?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, accountId);
			pst.setInt(2, userId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return false;
	}

	@Override
	public int getOpenAccountId(int userId) {
		
		try {
			
			String sql = "SELECT count(*) openAccountNum FROM account WHERE userid = ? AND statusid = 2;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return rs.getInt("openAccountNum");
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return 0;
	}


	@Override
	public boolean isEqualAccountId(int firstAccountId, int secondAccountId) {

		try {
			
			String sql = "SELECT * FROM account WHERE "
					+ "accountid = ? AND accountid = ?";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, firstAccountId);
			pst.setInt(2, secondAccountId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 		
		
		return false;
	}

	@Override
	public String getAccountStatus(int accountId) {
		String str = "";
		
		try {
			
			String sql = "SELECT a.status FROM accountstatus a \r\n" + 
					"INNER JOIN account b ON a.statusid = b.statusid \r\n" + 
					"WHERE b.accountid = ?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, accountId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return rs.getString("status");
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return str;
	}

	@Override
	public List<Account> getAllPendingAccounts() {
		List<Account> accounts = new ArrayList<>();
		
		try {
			String sql = "SELECT a.accountid, TRUNC(a.balance :: NUMERIC, 2) AS balance, \r\n" + 
					"b.status,b.statusid, c.typeid, c.TYPE, c.type FROM account a \r\n" + 
					"INNER JOIN accountstatus b ON a.statusid = b.statusid \r\n" + 
					"INNER JOIN accounttype c ON a.typeid = c.typeid \r\n" + 
					"WHERE b.status = 'Pending' ORDER BY a.accountid;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			ResultSet rs = pst.executeQuery();			
		
			while(rs.next()) {
				Account account = new Account();
				account.setAccountId(rs.getInt("accountid"));
				account.setBalance(rs.getDouble("balance"));
				account.setStatus(new AccountStatus(rs.getInt("statusid"), rs.getString("status")));
				account.setType(new AccountType(rs.getInt("typeId"), rs.getString("type")));				
				accounts.add(account);
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return accounts;
	}

	@Override
	public int getLastAccountId(int userId) {
		int accountId = -1;
		
		try {
				
			String sql = "SELECT MAX(accountid) As accountid FROM account a WHERE a.userid = ?";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return rs.getInt("accountid");
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return accountId;
	}

	@Override
	public boolean isAccountUpdated(int userId, Account account) {

		try {
			String sql = "UPDATE account SET balance=?, statusid=?, typeid=? \r\n" + 
					"WHERE accountid = ? AND userid=?;";
	
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			
			pst.setDouble(1, account.getBalance());
			pst.setInt(2, account.getStatus().getStatusId());
			pst.setInt(3, account.getType().getTypeId());
			pst.setInt(4, account.getAccountId());
			pst.setInt(5, userId);
			
			if(pst.executeUpdate() > 0)
				return true;
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return false;
	}

	@Override
	public int getUserIdInAccountByAccountId(int accountId) {
		
		try {
			
			String sql = "SELECT userid FROM account WHERE accountid = ?";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, accountId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return rs.getInt("userid");
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return -1;
	}


	@Override
	public boolean isValidStatusId(int statusId) {
		
		try {
			
			String sql = "SELECT * FROM accountstatus WHERE statusid = ?";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, statusId);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 		
		
		return false;
	}

	@Override
	public boolean isValidStatus(String status) {
		
		try {
			
			String sql = "SELECT * FROM accountstatus WHERE status = ?";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, status);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 		
		
		return false;

	}

	@Override
	public boolean isMatchStatusIdAndStatus(int statusId, String status) {
		try {
			
			String sql = "SELECT * FROM accountstatus WHERE statusid = ? AND status = ?";
				 
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, statusId);
			pst.setString(2, status);
			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 		
		
		return false;
	}

	@Override
	public boolean isValidTypeId(int typeId) {
		try {
			
			String sql = "SELECT * FROM accounttype WHERE typeid = ?";
				 
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, typeId);

			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 		
		
		return false;
	}

	@Override
	public boolean isValidType(String type) {
		try {
			
			String sql = "SELECT * FROM accounttype WHERE type = ?";
				 
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, type);

			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 		
		
		return false;
	}

	@Override
	public boolean isMatchTypeIdAndType(int typeId, String type) {
		try {
			
			String sql = "SELECT * FROM accounttype WHERE typeid = ? AND type=?";
				 
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, typeId);
			pst.setString(2, type);

			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 		
		
		return false;
	}

	@Override
	public List<LocalDate> accruedInterestForSavings(int numOfMonths) {
	
		List<LocalDate> dates = new ArrayList<>();
		
		try {
			
			String sql = "SELECT createdate FROM account a \n" + 
					"INNER JOIN accounttype b ON a.typeid = b.typeid WHERE b.TYPE='Savings';";
				 
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);


			ResultSet rs = pst.executeQuery();			
		
			while(rs.next()) {
				dates.add(rs.getDate("createdate").toLocalDate());
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 		
		
		return dates;
		
		
	}

	@Override
	public int getAccountTypeIdByAccountType(String accountType) {
		
		try {
			
			String sql = "SELECT typeid FROM accounttype WHERE type=?";
				 
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, accountType);

			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return rs.getInt("typeid");
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 		
		
		return 0;
	}

	@Override
	public boolean isMatchUserIdAndAccountId(int userId, int accountId) {
		
		try {
			
			String sql = "SELECT * FROM account WHERE userid=? AND accountid=?";
				 
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			pst.setInt(2, accountId);

			ResultSet rs = pst.executeQuery();			
		
			if(rs.next()) {
				return true;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 				
		
		return false;
	}
	
}
