
package com.bankingapi.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.servlethelper.UserRequestHelper;
import com.bankingapi.subservices.Message;
import com.fasterxml.jackson.databind.ObjectMapper;


public class UserServlet extends HttpServlet {
	
	private static UserServices userServices = new UserServicesImpl();	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
	
		HttpSession session = req.getSession();
		
		if (session.getAttribute("userId") == null || !req.getMethod().equals("GET")) {
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;			
		}
		
		UserRequestHelper.findUser(req, resp);			
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

			
		if (!req.getMethod().equals("POST")) {
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;			
		}
				
		UserRequestHelper.registerAndAuthentication(req, resp);	
	}	
	
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		HttpSession session = req.getSession();
		
		if (session.getAttribute("userId") == null || !req.getMethod().equals("PUT")) {
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;			
		}
				
		UserRequestHelper.updateUser(req, resp);
	}
	

}
