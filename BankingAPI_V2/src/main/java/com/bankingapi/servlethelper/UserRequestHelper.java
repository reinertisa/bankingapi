package com.bankingapi.servlethelper;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.controller.AuthenticationController;
import com.bankingapi.controller.UserController;
import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.subservices.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserRequestHelper {

	public static UserServices userServices = new UserServicesImpl();	
	
	public static void findUser(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {

		HttpSession session = req.getSession();
		
		if (session.getAttribute("userId") == null || !req.getMethod().equals("GET")) {
			
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;			
		}		
		
		switch(req.getRequestURI()) {
		
		case "/BankingAPI_V2/users":
			UserController.getAllUser(req, resp);
			return;	
		case "/BankingAPI_V2/users/":
			UserController.getUser(req, resp);
			return;		
		default: 
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;	
		}		
	}
	
	public static void registerAndAuthentication(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException, ServletException {
		
		if (!req.getMethod().equals("POST")) {
			
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;			
		}	
		
		
		
		switch(req.getRequestURI()) {
		
		case "/BankingAPI_V2/register":
			UserController.addUser(req, resp);
			return;
		case "/BankingAPI_V2/login":
			AuthenticationController.login(req, resp);
			return;
		case "/BankingAPI_V2/logout":
			AuthenticationController.logout(req, resp);
			return;	
		default: 
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;	
		}	
		
	}
	
	public static void updateUser(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();
		
		if (session.getAttribute("userId") == null || !req.getMethod().equals("PUT")) {
			
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;			
		}	
		
		switch(req.getRequestURI()) {
		
		case "/BankingAPI_V2/users":
			UserController.updateUser(req, resp);
			return;
			
		default: 
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;	
		}	
	}
	
}
