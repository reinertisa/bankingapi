package com.bankingapi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.model.Account;
import com.bankingapi.model.AccountStatus;
import com.bankingapi.model.User;
import com.bankingapi.services.AccountServices;
import com.bankingapi.services.AccountServicesImpl;
import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.subservices.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AccountController {
	
	private static AccountServices accountServices = new AccountServicesImpl();
	private static UserServices userServices = new UserServicesImpl();
	
	public static void getAllAccounts(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");		
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find All Accounts - Account Controller process started");
		
		if(role.equals("Admin") || role.equals("Employee")) {
			List<Account> accounts = accountServices.getAllAccounts();
			
			if(accounts.size() != 0) {
				
				int count = accountServices.numOfAccount();
				
				Message message = userServices.getMessage(count + " account(s) " + "found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				
				List<PrivateAccount> privateAccounts = new ArrayList<>();
				for(Account account : accounts) {
					
					PrivateAccount privateAccount = 
							new PrivateAccount(
								account.getAccountId(), account.getBalance(), 
								account.getStatus().getStatus(), account.getType().getType());
					privateAccounts.add(privateAccount);
				}
				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccounts));
			} else {
				Message message = userServices.getMessage("No account in the bank system yet");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}					
		} else {
			
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
	
	
	public static void getAccountById(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find Accounts By Id - Account Controller process started");
		
		Account searchAccount = null;
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			searchAccount = mapper.readValue(req.getInputStream(), Account.class);
		} catch (Exception e) {
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			
			System.out.println("Account class not created");
			return;
		}
		
		int searchId = searchAccount.getAccountId();
		
		if(role.equals("Admin") || role.equals("Employee") || 
					(role.equals("Standard") && accountServices.getAccountIdByUserId(userId) == searchId) ) {
		
			if(accountServices.hasAccount(searchId)) {
				
				Message message = userServices.getMessage("Account found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					
				Account result = accountServices.getAccountByAccountId(searchId);
					
				PrivateAccount privateAccount = 
						new PrivateAccount(result.getAccountId(), 
										   result.getBalance(), 
										   result.getStatus().getStatus(), 											   result.getType().getType());
						
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccount));
				System.out.println("Account found successfully");
			} else {
				Message message = userServices.getMessage("Invalid fields");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));					
	
				message = userServices.getMessage("Account not found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				System.out.println("Account not found in the system");
			}
			
		} else {
				Message message = userServices.getMessage("The requested action is not permitted");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));			
		}
	}	
	
	public static void getAccountByStatus(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find Accounts By Status - Account Controller process started");
		
		AccountStatus searchStatus = null;		
		try {
			ObjectMapper mapper = new ObjectMapper();
			searchStatus = mapper.readValue(req.getInputStream(), AccountStatus.class);
		} catch (Exception e) {
			System.out.println("Class not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			return;
		}
		
		int searchStatusId = searchStatus.getStatusId();		
		if(role.equals("Admin") || role.equals("Employee")) {
			
			List<Account> accounts = accountServices.getAllAccountsByStatus(searchStatusId);
			
			if(accounts.size() != 0) {			
				resp.getWriter().write(new ObjectMapper().writeValueAsString(accounts));
			
			} else {
				Message message = userServices.getMessage("There is no account with this status");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			}
			
		} else {			
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
	
	public static void getAccountByOwner(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find Accounts By User - Account Controller process started");
		
		User user = null;		
		try {
			ObjectMapper mapper = new ObjectMapper();
			user = mapper.readValue(req.getInputStream(), User.class);
			
		} catch (Exception e) {
			System.out.println("Class not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			return;
		}
		
		int searchUserId = user.getUserId();
			
		if(role.equals("Admin") || role.equals("Employee")) {
			
			List<Account> accounts = accountServices.getAccountsByUserId(searchUserId);
			
			if(accounts.size() != 0) {
				resp.getWriter().write(new ObjectMapper().writeValueAsString(accounts));
				
			} else {
				Message message = userServices.getMessage("There is no account with this userid");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}
					
		} else if(role.equals("Standard") && userId == searchUserId){
			
			List<Account> accounts = accountServices.getAccountsByUserId(searchUserId);
				
			if(accounts.size() != 0) {
				resp.getWriter().write(new ObjectMapper().writeValueAsString(accounts));
			} else {
				Message message = userServices.getMessage("There is no account with this userid");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}					
		}
	}
	
	public static void getAcccountByUserIdAndType(HttpServletRequest req, HttpServletResponse resp) {
		
	}
	
	public static void submitAccount(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);
		
		System.out.println("Submit Account - Account Controller process started");
		
		User user = null;		
		try {
			ObjectMapper mapper = new ObjectMapper();
			user = mapper.readValue(req.getInputStream(), User.class);
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		try {			
			
			if(role.equals("Admin") || role.equals("Employee")) {
				
				boolean isSubmitAccount = accountServices.isAccountCreated(userId, user.getAccounts().get(0));
				
				if(isSubmitAccount) {
					
					int lastAccountId = accountServices.getLastAccountId(userId);
					Account newAccount = accountServices.getAccountByAccountId(lastAccountId);
					
					resp.setStatus(HttpServletResponse.SC_CREATED);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(newAccount));
				
				} else {
					
					Message message = userServices.getMessage("Invalid fields");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					System.out.println("Invalid fields");
				}
				
			} else if(role.equals("Standard") && userId == user.getUserId()) {
				
				boolean isSubmitAccount = accountServices.isAccountCreated(userId, user.getAccounts().get(0));
				
				if(isSubmitAccount) {
					int lastAccountId = accountServices.getLastAccountId(userId);
					Account newAccount = accountServices.getAccountByAccountId(lastAccountId);
			
					resp.setStatus(HttpServletResponse.SC_CREATED);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(newAccount));
				
				} else {
					
					Message message = userServices.getMessage("Invalid fields");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					System.out.println("Invalid fields");					
				}				
			} 
		}catch(Exception e) {			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			System.out.println("Invalid field");
		}
		
	}
	
	public static void updateAccount(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);
		
		System.out.println("Update Account - Account Controller process started");
		
		Account willUpdateAccount = null;		
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			willUpdateAccount = mapper.readValue(req.getInputStream(), Account.class);
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		try {			
			
			Account account = accountServices.getAccountByAccountId(willUpdateAccount.getAccountId());
			
			if(account == null) {
				Message message = userServices.getMessage("Invalid fields");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				System.out.println("Updated account not found");
				
			} else {
				
				int accountId = account.getAccountId();
				int userIdInAccount = accountServices.getUserIdInAccountByAccountId(accountId);
				
				if(userIdInAccount == -1) {
					
					Message message = userServices.getMessage("Invalid fields");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					
				} else {
					
					boolean isAccountUpdated = accountServices.isAccountUpdated(userIdInAccount, willUpdateAccount);
					
					if(isAccountUpdated) {					
						Account updatedAccount = accountServices.getAccountByAccountId(accountId);
						resp.getWriter().write(new ObjectMapper().writeValueAsString(updatedAccount));
						System.out.println("Account updated successfully");
					} else {					
						Message message = userServices.getMessage("Invalid fields");
						resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					}					
					
				}
			}
			
		}catch(Exception e) {
			System.out.println("Invalid field");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
}

class PrivateAccount{
	
	int accountId;
	double balance;
	String status;
	String type;
	public PrivateAccount() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PrivateAccount(int accountId, double balance, String status, String type) {
		super();
		this.accountId = accountId;
		this.balance = balance;
		this.status = status;
		this.type = type;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "PrivateAccount [accountId=" + accountId + ", balance=" + balance + ", status=" + status + ", type="
				+ type + "]";
	}
	
}
