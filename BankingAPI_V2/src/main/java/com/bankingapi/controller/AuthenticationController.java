package com.bankingapi.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.model.User;
import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.subservices.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthenticationController {

	private static UserServices userServices = new UserServicesImpl();
	
	public static void login(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
			
		resp.setContentType("application/json");
		
		System.out.println("Login Authentication controller started");
		
		User user = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			user = mapper.readValue(req.getInputStream(), User.class);
		} catch (Exception e) {
			System.out.println("User class not created");
			
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			Message message = userServices.getMessage("Invalid Credentials");	
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			message = userServices.getMessage("You did not enter password or username");	
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}

		user = userServices.getUser(user.getUsername(), user.getPassword());
		

		
		if(user == null) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			Message message = userServices.getMessage("Invalid Credentials");	
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			message = userServices.getMessage("Password and username not matched");	
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		} 
		
		PrivateLoginUser privateLoginUser = 
				new PrivateLoginUser(user.getUserId(), user.getUsername(), 
									 user.getFirstName(), user.getLastName(), 
									 user.getEmail(), user.getRole().getRole());
		
		
		String loginResponse = "Welcome," + "{" + user.getUsername() + "} " +
							   "you are in the online banking system";
		
		Message message = userServices.getMessage(loginResponse);	
		resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		
		HttpSession session = req.getSession();
		session.setAttribute("userId", user.getUserId());
		resp.getWriter().write(new ObjectMapper().writeValueAsString(privateLoginUser));
		
	
			
		System.out.println("Session created");
		
	}
	
	public static void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		HttpSession session = req.getSession();	
		
		System.out.println("Logout Authentication controller started");
		
		if (session.getAttribute("userId") == null || !req.getMethod().equals("POST")) {
			
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			Message message = userServices.getMessage("There was no user logged into the session");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			System.out.println("There was no user logged into the session");
			
		} else {
			
			int userId = (int)session.getAttribute("userId");
			String username = userServices.getUsername(userId);
			
			String response = "You have successfully logged out {" + username + "}";
			
			Message message = userServices.getMessage(response);
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			session.removeAttribute("userId");
			session.invalidate();
			
			System.out.println("Session closed successfully");
		}		
	}	

}


class PrivateLoginUser{
	
	private int userId; 
	private String username; 
	private String firstName; 
	private String lastName; 
	private String email; 
	private String role;
	
	
	public PrivateLoginUser() {
		super();

	}
	public PrivateLoginUser(int userId, String username, String firstName, String lastName, String email,
			String role) {

		this.userId = userId;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.role = role;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "PrivateUser [userId=" + userId + ", username=" + username + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", role=" + role + "]";
	}
}


