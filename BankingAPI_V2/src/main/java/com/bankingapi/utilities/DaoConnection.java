package com.bankingapi.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DaoConnection {

	
	private static final String url = "jdbc:postgresql://"+System.getenv("TRAINING_DB_URL")+"/bankingapi";
	private static final String user = System.getenv("TRAINING_DB_USERNAME");
	private static final String password = System.getenv("TRAINING_DB_PASSWORD");
	private static Connection conn = null;
	
	public static Connection getConnection(){
		
		if(conn == null) {
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				System.out.println("could not register driver");
				e.printStackTrace();
			}
			
			try {
				conn = DriverManager.getConnection(url, user, password);
			} catch (SQLException e) {
				System.out.println("connection failed");
				e.printStackTrace();
			}
			
		}
		
		try {
			if(conn.isClosed()) {
				conn = DriverManager.getConnection(url, user, password);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return conn;
		
	}
	
	
}




