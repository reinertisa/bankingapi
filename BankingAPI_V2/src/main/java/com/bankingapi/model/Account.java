package com.bankingapi.model;

import java.time.LocalDate;

public class Account {

	private int accountId; // primary key
	private double balance;  // not null
	private AccountStatus status;
	private AccountType type;
	private LocalDate createDate;
	
	public Account() {		

	}
	
	public Account(double balance, AccountStatus status, AccountType type) {
		this.balance = balance;
		this.status = status;
		this.type = type;
		this.createDate = LocalDate.now();
	}
		
	public Account(int accountId, double balance, AccountStatus status, AccountType type) {
		this.accountId = accountId;
		this.balance = balance;
		this.status = status;
		this.type = type;
		this.createDate = LocalDate.now();
	}

	
	public Account(int accountId, double balance, AccountStatus status, AccountType type, LocalDate createDate) {
		this.accountId = accountId;
		this.balance = balance;
		this.status = status;
		this.type = type;
		this.createDate = LocalDate.now();
		
	}

	public AccountType getType() {
		return type;
	}

	public void setType(AccountType type) {
		this.type = type;
	}
	  
	public int getAccountId() {
		return accountId;
	}

	public AccountStatus getStatus() {
		return status;
	}
	
	public void setStatus(AccountStatus status) {
		this.status = status;
	}
	
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public LocalDate getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDate createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "Account [accountId=" + accountId + ", balance=" + balance + ", status=" + status + ", type=" + type
				+ ", createDate=" + createDate + "]";
	}
	
	
}
