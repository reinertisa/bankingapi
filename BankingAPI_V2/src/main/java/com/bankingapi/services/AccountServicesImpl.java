package com.bankingapi.services;

import java.util.List;

import com.bankingapi.dao.AccountDao;
import com.bankingapi.dao.AccountDaoImpl;
import com.bankingapi.dao.UserDao;
import com.bankingapi.model.Account;
import com.bankingapi.model.AccountStatus;
import com.bankingapi.model.AccountType;


public class AccountServicesImpl implements AccountServices{

	private AccountDao accountDao = new AccountDaoImpl();
	private UserServices userServices = new UserServicesImpl();	

	@Override
	public boolean isAccountCreated(int userId, Account account) {
		
		if(!userServices.hasUser(userId)) {
			return false;
		}
						
		if(account.getBalance() < 0) {
			return false;
	
		}
		
		/* you can add status role control later*/
		
		return accountDao.isAccountCreated(userId, account);			
	}	
	
	
	@Override
	public boolean isAccountUpdated(int userId, Account account) {
		
		if(account.getBalance() < 0) {
			return false;
	
		}
		
		return accountDao.isAccountUpdated(userId, account);
	}
	

	@Override
	public List<Account> getAllAccounts() {
		
		return accountDao.getAllAccounts();
	}

	@Override
	public boolean hasEnoughBalanceForWithdrawal(int accountId, double amount) {
		
		return accountDao.getBalanceByAccountId(accountId) >= amount;
	}

	

	@Override
	public boolean hasEnoughOpenAccounts(int userId) {
		
		return accountDao.getOpenAccountId(userId) > 0;		
	}
	
	public boolean hasEnoughOpenAccountsForTransferMoneyInYourAccounts(int userId) {
		
		return accountDao.getOpenAccountId(userId) > 1;		
	}

	@Override
	public boolean isEqualAccountId(int firstAccount, int secondAccount) {
		return accountDao.isEqualAccountId(firstAccount, secondAccount);
	}


	@Override
	public boolean isOpenAccount(int accountId) {
		return accountDao.getAccountStatus(accountId).equals("Open");
	}


	@Override
	public boolean isPendingAccount(int accountId) {
		return accountDao.getAccountStatus(accountId).equals("Pending");
	}


	@Override
	public boolean updateAccountStatusOpen(int accountId) {
		return accountDao.updateAccountStatusByOpen(accountId);
	}


	@Override
	public boolean updateAccountStatusDenied(int accountId) {

		return accountDao.updateAccountStatusByDenied(accountId);
	}
	
	@Override
	public List<Account> getAccountsByUserId(int userId) {
		
		return accountDao.getAllAccountsByUserId(userId);

	}

	@Override
	public int getAccountIdByUserId(int userId) {
		return accountDao.getAccountIdByUserId(userId);
	}

//	@Override
//	public boolean isValidAccount(int accountId) {
//		return accountDao.getAccountIdbyAccountId(accountId);
//	}

	@Override
	public boolean isOpenAccount(int accountId, int userId) {
		return accountDao.getAccountStatusByAccountId(accountId, userId).equals("Open");
	}

	@Override
	public boolean isAccountOwnedByUser(int accountId, int userId) {
		return accountDao.isAccountOwnedByUser(accountId, userId);
	}


	@Override
	public boolean createCheckingAccount(int userId, int typeId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean createSavingsAccount(int userId, int typeId) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void deleteAccount() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<Account> getAllAccountsByStatus(int statusId) {
		return accountDao.getAllAccountsByStatusId(statusId);
	}

	@Override
	public Account getAccountByAccountId(int accountId) {
		return accountDao.getAccountByAccountId(accountId);
	}

	@Override
	public boolean hasAccount(int accountId) {
		return accountDao.hasAccountIdbyAccountId(accountId);
	}


	@Override
	public int getLastAccountId(int userId) {
		return accountDao.getLastAccountId(userId);
	}


	@Override
	public boolean isTransferFunds(int sourceAccountId, int targetAccountId, double amount) {
		
		
		if(isEqualAccountId(sourceAccountId, targetAccountId)) {
			return false;
		}
	
		if(!isMakeAWithdrawal(sourceAccountId, amount)) {
			return false;
		}
		
		if(!isMakeADeposit(targetAccountId, amount)) {
			return false;
		}
		
		return true;
	}
	
	public boolean makeTransferFunds(int sourceAccountId, int targetAccountId, double amount) {
		
		return makeAWithdrawal(sourceAccountId, amount) &&
			   makeADeposit(targetAccountId, amount);
	}


	@Override
	public boolean isMakeAWithdrawal(int accountId, double amount) {
		
		if(amount <= 0) {
			return false;
		}
		
		if(!hasAccount(accountId)) {
			return false;
		}
		
		if(!isOpenAccount(accountId)) {
			return false;
		}
		
		if(!hasEnoughBalanceForWithdrawal(accountId, amount)) {
			return false;
		}
		
		return true;
				
	}
	
	public boolean makeAWithdrawal(int accountId, double amount) {
		return accountDao.makeAWithdrawal(accountId, amount);
	}

	@Override
	public boolean isMakeADeposit(int accountId, double amount) {
		
		if(amount <= 0) {
			return false;
		}
		
		if(!hasAccount(accountId)) {
			return false;
		}
		
		if(!isOpenAccount(accountId)) {
			return false;
		}
	
		return true;
	}
	
	public boolean makeADeposit(int accountId, double amount) {
		return accountDao.makeADeposit(accountId, amount);
	}


	@Override
	public int getUserIdInAccountByAccountId(int accountId) {
		
		return accountDao.getUserIdInAccountByAccountId(accountId);
	
	}


	@Override
	public int numOfAccount() {
		return accountDao.numOfAccount();
	}


}


