<------BANKING APP DB----------> 

DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS accountstatus;
DROP TABLE IF EXISTS accounttype;
COMMIT;

<-------------BANKUSER TABLE-------->
CREATE TABLE users(
	
		userid SERIAL PRIMARY KEY,
		username VARCHAR(50) NOT NULL UNIQUE,
		password VARCHAR(50) NOT NULL,
		firstname VARCHAR(50) NOT NULL,
		lastname VARCHAR(50) NOT NULL,
		email VARCHAR(50) NOT NULL,
		roleid INTEGER NOT NULL
		
);

SELECT * FROM users;

<-------------ROLE TABLE------------>

CREATE TABLE role(
		roleid INTEGER NOT NULL PRIMARY KEY,
		role VARCHAR(50) NOT NULL
);

INSERT INTO ROLE (roleid, role) VALUES (1, 'Admin');
INSERT INTO ROLE (roleid, role) VALUES (2, 'Employee');
INSERT INTO ROLE (roleid, role) VALUES (3, 'Standard');
INSERT INTO ROLE (roleid, role) VALUES (4, 'Premium');

SELECT * FROM role;

<-------------ACCOUNT TYPE TABLE-------->

CREATE TABLE accounttype(
	typeid SERIAL PRIMARY KEY,
	type VARCHAR(50) NOT NULL UNIQUE  
);

INSERT INTO accounttype (type) VALUES ('Checking');
INSERT INTO accounttype (type) VALUES ('Savings');

SELECT * FROM accounttype;

<-------------ACCOUNT STATUS TABLE-------->

CREATE TABLE accountstatus(
	statusid SERIAL PRIMARY KEY,
	status VARCHAR(50) NOT NULL UNIQUE
);

INSERT INTO accountstatus (status) VALUES('Pending');
INSERT INTO accountstatus (status) VALUES('Open');
INSERT INTO accountstatus (status) VALUES('Closed');
INSERT INTO accountstatus (status) VALUES('Denied');

SELECT * FROM accountstatus;

<-------------ACCOUNT TABLE---------------->

CREATE TABLE account(
	accountid SERIAL PRIMARY KEY,
	balance DOUBLE PRECISION NOT NULL,
	
	userid INTEGER NOT NULL,
	CONSTRAINT fk_users
	FOREIGN KEY (userid) REFERENCES users(userid),
	
	statusid INTEGER NOT NULL,
	CONSTRAINT fk_accountstatus
	FOREIGN KEY (statusid) REFERENCES accountstatus(statusid),

	typeid INTEGER NOT NULL,
	CONSTRAINT fk_accounttype
	FOREIGN KEY (typeid) REFERENCES accounttype(typeid),
	
	createDate date

);

SELECT * FROM account;

SELECT * FROM users;
SELECT * FROM ROLE;



SELECT a.userid, a.username, a.PASSWORD, a.firstname, a.lastname, a.email, 
b.roleid, b.role, c.accountid, trunc(c.balance :: NUMERIC, 2) AS balance, 
d.statusid, d.status, e.typeid , e.type  FROM users a
INNER JOIN role b ON a.roleid = b.roleid 
INNER JOIN account c ON a.userid = c.userid 
INNER JOIN accountstatus d ON c.statusid = d.statusid 
INNER JOIN accounttype e ON c.typeid = e.typeid WHERE a.userid = 1;


SELECT a.userid, a.username, a.PASSWORD, a.firstname, a.lastname, a.email, b.roleid, b.role FROM users 
INNER JOIN role b ON a.roleid = b.roleid WHERE a.userid = 1;


SELECT a.userid, a.username, a.password, a.firstname, a.lastname, a.email , a.roleid , b.role FROM users a
LEFT OUTER JOIN ROLE b ON a.roleid = b.roleid WHERE userid=5;

SELECT * FROM users;

SELECT a.userid, a.username, a.password, a.firstname, 
a.lastname, a.email , a.roleid , b.role FROM users a
LEFT OUTER JOIN ROLE b ON a.roleid = b.roleid WHERE userid=11;



SELECT a.userid, a.username, a.password, a.firstname, 
a.lastname, a.email , a.roleid , b.role FROM users a
LEFT OUTER JOIN ROLE b ON a.roleid = b.roleid WHERE userid=11;

SELECT * FROM users;

SELECT * FROM ROLE;

SELECT * FROM account;

INSERT INTO account

SELECT * FROM users a LEFT JOIN ROLE b ON a.roleid = b.roleid;

SELECT role FROM role a 
INNER JOIN users b ON a.roleid = b.roleid WHERE userid=6;

INSERT INTO users values(6,'kristi', '123', 'kristi', 'reinert', 'kristimail',1);

SELECT userid FROM users WHERE username = 'x' AND PASSWORD = 'b';

SELECT * FROM users a 
INNER JOIN ROLE b ON a.roleid = b.roleid 
INNER JOIN account c ON a.userid = c.userid 
INNER JOIN accountstatus d ON d.statusid = c.statusid 
INNER JOIN accounttype e ON e.typeid = c.typeid; 

UPDATE users SET username='m', PASSWORD='m', 
firstname='m', lastname='m', email='g' WHERE userid=7;

SELECT * FROM account;

SELECT * FROM USERs;

INSERT INTO account values(10,100.23455,18,2,2);

SELECT a.accountid, TRUNC(a.balance :: NUMERIC, 2) AS balance, 
b.statusid , b.status , c.typeid, c.type FROM account a 
INNER JOIN accountstatus b ON a.statusid = b.statusid 
INNER JOIN accounttype c ON a.typeid = c.typeid 
WHERE a.accountid = 6;


SELECT * FROM account;

UPDATE account SET balance=1000, statusid = 1, typeid=1 
WHERE accountid = 14 AND userid=1;

SELECT * FROM users;
SELECT * FROM account;

SELECT * FROM ROLE;

SELECT * FROM ROLE WHERE ROLE='Employee' AND roleid=2;


COMMIT;