package com.bankingapi.services;

import java.util.List;

import com.bankingapi.dao.UserDao;
import com.bankingapi.dao.UserDaoImpl;
import com.bankingapi.model.User;

public class UserServicesImpl implements UserServices{

	private UserDao userDao = new UserDaoImpl();

	
	@Override
	public String registerUser(User newUser) {
		
		String result= confirmUserInfo(newUser);
		
		if(!result.equals("confirmationSuccess")) {
			return result;
		}
		
		if(userDao.isCreateUser(newUser)) {
			return "success";
		} else {
			return "user not created";
		}
	}	
	
	
	@Override
	public String updateUser(User user) {
		
		if(!userDao.hasUser(user.getUserId())) {
			return "User not found in the system";
		}
		
		if(!isValidUsername(user.getUsername())) {
			return "Invalid username";
		}
		
		if(!userDao.isAvailableUsername(user.getUsername())) {
			if(!userDao.isYourUsername(user.getUsername(), user.getUserId())) {
				return "Username not available";
			}
		}	
		
		if(!isValidPassword(user.getPassword())) {
			return "Invalid password";
		}
		
		if(!isValidFirstName(user.getFirstName())) {
			return "Invalid firstname";
		}
		
		if(!isValidLastName(user.getLastName())) {
			return "Invalid lastname";
		}
		
		if(!isValidEmail(user.getEmail())) {
			return "Invalid email";
		}
		
		if(!userDao.isAvailableEmail(user.getEmail())) {
			if(!userDao.isYourEmail(user.getEmail(), user.getUserId())) {
				return "Email not available";
			}
		}
		
		if(!isValidRoleId(user.getRole().getRoleId())){
			return "Invalid roleid";
		}
		
		if(!isMatchRoleIdAndRole(user.getRole().getRoleId(), user.getRole().getRole())) {
			return "Roleid and Role not matched";				
		}		
		
	
		if(userDao.updateUser(user)) {
			return "success";
		} else {
			return "user not updated";
		}	
	}
	
	public String confirmUserInfo(User user) {
		
		if(!isValidUsername(user.getUsername())) {
			return "Invalid username";
		}
		
		if(!isAvailableUsername(user.getUsername())) {
			return "Username not available";
		}
		
		if(!isValidPassword(user.getPassword())) {
			return "Invalid password";
		}
		
		if(!isValidFirstName(user.getFirstName())) {
			return "Invalid firstname";
		}
		
		if(!isValidLastName(user.getLastName())) {
			return "Invalid lastname";
		}
		
		if(!isValidEmail(user.getEmail())) {
			return "Invalid email";
		}
		
		if(!isAvailableEmail(user.getEmail())) {
			return "Email not available";
		}			
		
		if(!isValidRoleId(user.getRole().getRoleId())){
			return "Invalid roleid";
		}
		
		if(!isMatchRoleIdAndRole(user.getRole().getRoleId(), user.getRole().getRole())) {
			return "Roleid and Role not matched";				
		}
		
		return "confirmationSuccess";
		
	}
	
	@Override
	public User getUser(int userId) {
		
		if(userDao.hasUser(userId)) {
			return userDao.getUser(userId);
		}		
		return null;		
	}

	@Override
	public User getUser(String username, String password) {
		
		int userId = userDao.getUserId(username, password);
		if(userId == -1) {
			return null;
		} else {
			return userDao.getUser(userId);	
		}				
	}


	@Override
	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}

//	@Override
//	public User getUser(String searchId) {
//		
//		try {
//			return userDao.getUserByUserId(Integer.parseInt(searchId));
//		} catch (Exception e) {
//			System.out.println("Exception: searchid not found in getUser");
//		}
//	
//		return null;	
//	}

	@Override
	public Message getMessage(String message) {		
		return new Message(message);
	}
	
	@Override
	public String getUserRole(int userId) {		
		return userDao.getRolebyUserId(userId);
	}

	@Override
	public boolean hasUser(int userId) {

		return userDao.hasUser(userId);
	}

	@Override
	public String getUsername(int userId) {
		return userDao.getUsername(userId);
	}
	
	@Override
	public boolean isAvailableUsername(String username) {
		
		return !userDao.isAvailableUsername(username) ? false : true;

	}
	
	@Override
	public boolean isAvailableEmail(String email) {
		
		return !userDao.isAvailableEmail(email) ? false : true;
	}
	
	@Override
	public boolean isValidUsername(String username) {
		
		return (username == null || username.trim().length() == 0) ? false : true;
	}
	
	@Override
	public boolean isValidPassword(String password) {
		
		return (password == null || password.trim().length() == 0) ? false : true;	
	}
	
	@Override
	public boolean isValidFirstName(String firstName) {
		
		return (firstName == null || firstName.trim().length() == 0) ? false : true;
	}
	
	@Override
	public boolean isValidLastName(String lastName) {
		
		return (lastName == null || lastName.trim().length() == 0) ? false : true;
	
	}
	
	@Override
	public boolean isValidEmail(String email) {
		
		return (email == null || email.trim().length() == 0) ? false : true;		
	}
	
	@Override
	public boolean isValidRoleId(int roleId) {
	
		return userDao.isValidRoleId(roleId);
	}

	@Override
	public boolean isMatchRoleIdAndRole(int roleId, String role) {
		return userDao.isMatchRoleIdAndRole(roleId, role);
		
	}
		
	@Override
	public int numOfUser() {
		return userDao.numOfUser();		
	}
 }
 