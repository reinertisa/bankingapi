package com.bankingapi.services;

import java.util.List;

import com.bankingapi.model.User;

public interface UserServices {
	
	
	public String registerUser(User newUser);
	
	public User getUser(int userId);
	public User getUser(String username, String password);
	public List<User> getAllUsers();
	public String getUserRole(int userId);
	public String getUsername(int userId);
	
	public String updateUser(User user);
	public boolean hasUser(int userId);
	
	public Message getMessage(String message);
	
	public boolean isAvailableUsername(String username);
	public boolean isAvailableEmail(String email);
	public boolean isValidRoleId(int roleId);
	public boolean isMatchRoleIdAndRole(int roleId, String role);
	
	public boolean isValidUsername(String username);
	public boolean isValidPassword(String password);
	public boolean isValidFirstName(String firstName);	
	public boolean isValidLastName(String lastName);	
	public boolean isValidEmail(String email);
	
	public int numOfUser();
	


}
