package com.bankingapi.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.model.Account;
import com.bankingapi.model.AccountStatus;
import com.bankingapi.model.User;
import com.bankingapi.services.AccountServices;
import com.bankingapi.services.AccountServicesImpl;
import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.services.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AccountController {
	
	private static AccountServices accountServices = new AccountServicesImpl();
	private static UserServices userServices = new UserServicesImpl();
	
	public static void getAllAccounts(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");		
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find All Accounts - Account Controller process started");
		
		if(role.equals("Admin") || role.equals("Employee")) {
			List<Account> accounts = accountServices.getAllAccounts();
			
			if(accounts.size() != 0) {
				
				int count = accounts.size();
				
				Message message = userServices.getMessage(count + " account(s) " + "found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				
				List<PrivateAccount> privateAccounts = new ArrayList<>();
				for(Account account : accounts) {
					
					PrivateAccount privateAccount = 
							new PrivateAccount(
								account.getAccountId(), account.getBalance(), 
								account.getStatus().getStatus(), account.getType().getType());
					privateAccounts.add(privateAccount);
				}
				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccounts));
			} else {
				Message message = userServices.getMessage("No account in the bank system yet");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}					
		} else {
			
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
	
	
	public static void getAccountById(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find Accounts By Id - Account Controller process started");
		
		Account searchAccount = null;
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			searchAccount = mapper.readValue(req.getInputStream(), Account.class);
		} catch (Exception e) {
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			
			System.out.println("Account class not created");
			return;
		}
		
		int searchId = searchAccount.getAccountId();
		
		if(role.equals("Admin") || role.equals("Employee") || 
					(role.equals("Standard") && accountServices.getAccountIdByUserId(userId) == searchId) ) {
		
			if(accountServices.hasAccount(searchId)) {
				
				Message message = userServices.getMessage("Account found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					
				Account result = accountServices.getAccountByAccountId(searchId);
					
				PrivateAccount privateAccount = 
						new PrivateAccount(result.getAccountId(), 
										   result.getBalance(), 
										   result.getStatus().getStatus(), 											   result.getType().getType());
						
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccount));
				System.out.println("Account found successfully");
			} else {
				Message message = userServices.getMessage("Invalid fields");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));					
	
				message = userServices.getMessage("Account not found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				System.out.println("Account not found in the system");
			}
			
		} else {
				Message message = userServices.getMessage("The requested action is not permitted");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));			
		}
	}	
	
	public static void getAccountByStatus(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find Accounts By Status - Account Controller process started");
		
		if(!(role.equals("Admin") || role.equals("Employee"))) {
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		AccountStatus searchStatus = null;		
		try {
			ObjectMapper mapper = new ObjectMapper();
			searchStatus = mapper.readValue(req.getInputStream(), AccountStatus.class);
		} catch (Exception e) {
			System.out.println("Class not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			return;
		}
		
		int searchStatusId = searchStatus.getStatusId();		
		
		List<Account> accounts = accountServices.getAllAccountsByStatus(searchStatusId);
		
		if(accounts.size() != 0) {		
			int count = accounts.size();
			String status = accounts.get(0).getStatus().getStatus();
			Message message = userServices.getMessage(count + " " + status + " account(s) " + "found in the system");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			List<PrivateAccount> privateAccounts = new ArrayList<>();
			for(Account account : accounts) {
				
				PrivateAccount privateAccount = 
						new PrivateAccount(
							account.getAccountId(), account.getBalance(), 
							account.getStatus().getStatus(), account.getType().getType());
				privateAccounts.add(privateAccount);
			}
			
			resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccounts));
			
			System.out.println("Account found successfully");
			
		} else {
			Message message = userServices.getMessage("There is no account with this status");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
		}	
	}
	
	public static void getAccountByOwner(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("Find Accounts By User - Account Controller process started");
		
		User user = null;		
		try {
			ObjectMapper mapper = new ObjectMapper();
			user = mapper.readValue(req.getInputStream(), User.class);
			
		} catch (Exception e) {
			System.out.println("Class not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			return;
		}
		
		int searchUserId = user.getUserId();
			
		if(role.equals("Admin") || role.equals("Employee") || (role.equals("Standard") && userId == searchUserId)) {
			
			List<Account> accounts = accountServices.getAccountsByUserId(searchUserId);
			
			if(accounts.size() != 0) {
				
				int count = accounts.size();
				
				Message message = userServices.getMessage(count + " account(s) " + "found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
											
				List<PrivateAccount> privateAccounts = new ArrayList<>();
				for(Account account : accounts) {
					
					PrivateAccount privateAccount = 
							new PrivateAccount(
								account.getAccountId(), account.getBalance(), 
								account.getStatus().getStatus(), account.getType().getType());
					privateAccounts.add(privateAccount);
				}
				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccounts));
				
				System.out.println("Account found successfully");			
				
			} else {
				Message message = userServices.getMessage("There is no account with this userid");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}
					
		} else {
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}
	}
	
	public static void getAcccountByUserIdAndType(HttpServletRequest req, HttpServletResponse resp) {
		
	}
	
	public static void submitAccount(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);
		
		System.out.println("Submit Account - Account Controller process started");
		
		Account account = null;		
		try {
			ObjectMapper mapper = new ObjectMapper();
			account = mapper.readValue(req.getInputStream(), Account.class);
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		try {		
			
			String output = accountServices.isAccountCreated(userId, account);
				
			if(output.equals("success")) {
					
					int lastAccountId = accountServices.getLastAccountId(userId);
					Account newAccount = accountServices.getAccountByAccountId(lastAccountId);
					
					Message message = userServices.getMessage("Account created successfully");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					
					PrivateAccount privateAccount = 
							new PrivateAccount(newAccount.getAccountId(), newAccount.getBalance(), 
											   newAccount.getStatus().getStatus(), 
											   newAccount.getType().getType());
					
					resp.setStatus(HttpServletResponse.SC_CREATED);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccount));
				
			} else {
					
					Message message = userServices.getMessage(output);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					System.out.println(output);
					
			} 
		}catch(Exception e) {			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			System.out.println("Invalid field");
		}
		
	}
	
	public static void updateAccount(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);
		
		System.out.println("Update Account - Account Controller process started");
		
		Account willUpdateAccount = null;		
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			willUpdateAccount = mapper.readValue(req.getInputStream(), Account.class);
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		try {			
			
			Account account = accountServices.getAccountByAccountId(willUpdateAccount.getAccountId());
			
			if(account == null) {
				Message message = userServices.getMessage("Account not found in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				System.out.println("Updated account not found");
				
			} else {
				
				int accountId = account.getAccountId();
				int userIdInAccount = accountServices.getUserIdInAccountByAccountId(accountId);
							
				String output = accountServices.isAccountUpdated(userIdInAccount, willUpdateAccount);
				
				if(output.equals("success")) {			
					
					Account updatedAccount = accountServices.getAccountByAccountId(accountId);
					
					Message message = userServices.getMessage("Account updated successfully");
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					
					PrivateAccount privateAccount = 
							new PrivateAccount(updatedAccount.getAccountId(), updatedAccount.getBalance(), 
									updatedAccount.getStatus().getStatus(), 
									updatedAccount.getType().getType());
					resp.getWriter().write(new ObjectMapper().writeValueAsString(privateAccount));
					
					System.out.println("Account updated successfully");
						
				} else {					
					Message message = userServices.getMessage(output);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				}					
					
			}
			
		}catch(Exception e) {
			System.out.println("Invalid field");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
	
	
	public static void passTime(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);	
		
		System.out.println("PassTime - Account Controller process started");
		
		if(!role.equals("Admin")){
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		SavingsInterest accruedInterest = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			accruedInterest = mapper.readValue(req.getInputStream(), SavingsInterest.class);
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		
		int numOfAccruedInterestSavings = accountServices.numofMonthsForSavings(accruedInterest.getNumOfMonths());
		
		String response = "{" + accruedInterest.getNumOfMonths() + "} " + 
						  "months of interest has been accrued for " + 
						  "{" + numOfAccruedInterestSavings + "} Savings Accounts";
		
		Message message = userServices.getMessage(response);
		resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		
		
	}
}

class PrivateAccount{
	
	int accountId;
	double balance;
	String status;
	String type;
	public PrivateAccount() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PrivateAccount(int accountId, double balance, String status, String type) {
		super();
		this.accountId = accountId;
		this.balance = balance;
		this.status = status;
		this.type = type;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "PrivateAccount [accountId=" + accountId + ", balance=" + balance + ", status=" + status + ", type="
				+ type + "]";
	}
	
}

class SavingsInterest{
	int numOfMonths;

	public SavingsInterest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SavingsInterest(int numOfMonths) {
		super();
		this.numOfMonths = numOfMonths;
	}

	public int getNumOfMonths() {
		return numOfMonths;
	}

	public void setNumOfMonths(int numOfMonths) {
		this.numOfMonths = numOfMonths;
	}
	
	
}
