package com.bankingapi.subservices;

import com.bankingapi.dao.AccountDao;
import com.bankingapi.dao.AccountDaoImpl;


public class AccountValidationControl {

	AccountDao accountDao = new AccountDaoImpl();
	
	public boolean isValidAccountId(String accountId) {

		int accountNum = 0;
		boolean result = false;
		
		try{
			accountNum = Integer.parseInt(accountId);
			result = true;
		} catch(NumberFormatException e) {
			System.out.println("Not Integer");
		}
		
		return result && accountDao.hasAccountIdbyAccountId(accountNum);

	}
	
	public boolean isInteger(String accountId) {
		
		boolean result = false;		
		try {
			Integer.parseInt(accountId);
			result=true;			
		}catch (NumberFormatException e) {			
			System.out.println("Not Integer");
		} 			
		return result;
	}

	
	public boolean isYourAccountNum(int accountId, int userId) {
		return accountDao.isAccountOwnedByUser(accountId, userId);
	}
	
	public boolean isSomeoneElseAccountNum(int accountId, int userId) {
		return !accountDao.isAccountOwnedByUser(accountId, userId);
	}			
	
	public boolean isOpenAccount(int accountId, int userId) {
		return !accountDao.getAccountStatusByAccountId(accountId, userId).equals("Open");
	}
	
	public boolean isOpenAccount(int accountId) {
		return accountDao.getAccountStatus(accountId).equals("Open");
	}
	
	public boolean isPendingAccount(int accountId) {
		return accountDao.getAccountStatus(accountId).equals("Pending");
	}
	
	public boolean isValidMoney(String money, int userId) {
		boolean result = false;
		
		try {
			double amount = Double.parseDouble(money);	
			
			if(amount <= 0.0D) {
				return false;
			}	
			result = true;		
			
		} catch (NumberFormatException e) { 
				System.out.println("\nOOPS >>>>> Not a valid number!");	
		}
		return result;
	}
	
	public boolean hasEnoughMoneyInAccount(int accountId, double amount) {
		return accountDao.getBalanceByAccountId(accountId) >= amount;
	}
	
	public boolean isEmptyString(String str) {
		str = str.trim();
		if(str == null || str.length() == 0) {
			return true;
		}
		return false;
	}
}