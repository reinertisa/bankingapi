package com.bankingapi.subservices;

import com.bankingapi.dao.UserDao;
import com.bankingapi.dao.UserDaoImpl;

public class UserValidationControl {
	
	UserDao userDao = new UserDaoImpl();
	
	public boolean isAvailableUsername(String username) {
		
		username = username.trim();
		
		if(username == null || username.length() == 0) {
			return false;
		}
		
		if(!userDao.isAvailableUsername(username)){
			return false;
		}
		
		return true;
	}
	
	public boolean isValidPassword(String password) {
		return password.trim().length() != 0;
	}
	
	public boolean isValidFirstName(String firstName) {
		
		return firstName.trim().length() != 0;
	}
	
	public boolean isValidLastName(String lastName) {
		
		return lastName.trim().length() != 0;
	}
	
	public boolean isValidEmail(String email) {
		
		email = email.trim();
		
		if(email == null || email.length() == 0) {
			return false;
		}
		
		if(!userDao.isAvailableEmail(email)){
			return false;
		}
		return true;
	}
	
	public boolean isValidRoleId(int roleId) {
	
		return userDao.isValidRoleId(roleId);
	}
	
	
	public int getInteger(String str) {
		int val = 0;
		System.out.println("String = " + str);

		try {
			val = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			System.out.println("Invalid String");
		}
		return val;
	}
	
	
}
