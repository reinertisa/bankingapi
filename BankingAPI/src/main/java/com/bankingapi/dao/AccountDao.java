package com.bankingapi.dao;

import java.util.List;

import com.bankingapi.model.Account;


public interface AccountDao {

	public boolean isAccountCreated(int userid, Account account);
	
	public List<Account> getAllAccounts();
	public List<Account> getAllAccountsByStatusId(int statusId);
	public List<Account> getAllAccountsByType(int typeId);
	public List<Account> getAllPendingAccounts();

	public List<Account> getAllAccountsByUserId(int userId);
	public Account getAccountByAccountId(int accountId);
	public List<Account> getAllAccountsByStatusId(int userId, int statusId);
	public List<Account> getAllAccountsByRole(int roleId);
	

	public boolean isAccountUpdated(int userId, Account account);
	public void updateAccountTypeByAccountId(int accountId, int typeId);
	public void updateAccountStatusByAccountId(int accountId, int statusId);
	public boolean updateAccountStatusByOpen(int accountId);
	public boolean updateAccountStatusByDenied(int accountId);
	
	
	public void deleteAccountByAccountId(int accountId);
	public void deleteAccountByUserId(int userId);
	
	
	public double getBalanceByAccountId(int accountId);
	public boolean makeAWithdrawal(int accountId, double amount);
	public boolean makeADeposit(int accountId, double amount);
	public boolean transferFunds(int firstAccountId, int secondAccountId, double amount);
	
	public int getAccountIdByUserId(int userId);
	public boolean hasAccountIdbyAccountId(int accountId);
	public String getAccountStatusByAccountId(int accountId, int userId);
	public String getAccountStatus(int accountId);
	public boolean isAccountOwnedByUser(int accountId, int userId);
	public int getLastAccountId(int userId);
	
	public int getOpenAccountId(int userId);
	
	public boolean isEqualAccountId(int firstAccountId, int secondAccountId);
	public int getUserIdInAccountByAccountId(int accountId);
	
}
