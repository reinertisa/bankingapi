package com.bankingapi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bankingapi.model.Account;
import com.bankingapi.model.AccountStatus;
import com.bankingapi.model.AccountType;
import com.bankingapi.model.Role;
import com.bankingapi.model.User;
import com.bankingapi.utilities.DaoConnection;

public class UserDaoImpl implements UserDao {
	
	Connection conn = null;
	PreparedStatement pst = null;

	@Override
	public HashMap<Integer, User> getAllUsers() {
		
		HashMap<Integer, User> map = new HashMap<>();
		
		try {
			String sql = "SELECT * FROM users a \r\n" + 
					"INNER JOIN ROLE b ON a.roleid = b.roleid \r\n" + 
					"INNER JOIN account c ON a.userid = c.userid \r\n" + 
					"INNER JOIN accountstatus d ON d.statusid = c.statusid \r\n" + 
					"INNER JOIN accounttype e ON e.typeid = c.typeid; ";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			ResultSet rs = pst.executeQuery();			
		
			while(rs.next()) {
				
				if(!map.containsKey(rs.getInt("userid"))) {
					User user = new User();					
				
					user.setUserId(rs.getInt("userid"));
					user.setUsername(rs.getString("username"));
					user.setPassword(rs.getString("password"));
					user.setFirstName(rs.getString("firstname"));
					user.setLastName(rs.getString("lastname"));
					user.setEmail(rs.getString("email"));
					user.setRole(new Role(rs.getInt("roleid"), rs.getString("role")));		
					
					List<Account> accounts = new ArrayList<>();
					Account account = new Account(rs.getInt("accountid"), 
							  rs.getDouble("balance"), 
							  new AccountStatus(rs.getInt("statusid"), rs.getString("status")),
							  new AccountType(rs.getInt("typeid"), rs.getString("type")));
					
					accounts.add(account);
					user.setAccounts(accounts);

					map.put(user.getUserId(), user);
					
				} else {
					User user = map.get(rs.getInt("userid"));
					List<Account> accounts = user.getAccounts();
					
					Account account = new Account(rs.getInt("accountid"), 
							  rs.getDouble("balance"), 
							  new AccountStatus(rs.getInt("statusid"), rs.getString("status")),
							  new AccountType(rs.getInt("typeid"), rs.getString("type")));
					accounts.add(account);
					
				}
				
			} 
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return map;
	}

	@Override
	public List<User> getAllUsersByRoleId(int roleId) {
		List<User> users = new ArrayList<>();
		
		try {
			String sql = "SELECT * FROM users a \r\n" + 
					"INNER JOIN role b ON a.roleid = b.roleid \r\n" + 
					"FULL OUTER JOIN account c ON a.userid = c.userid \r\n" + 
					"FULL OUTER JOIN accountstatus d ON c.statusid = d.statusid \r\n" + 
					"FULL OUTER JOIN accounttype e ON c.typeid = e.typeid WHERE a.roleid = ?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, roleId);
			ResultSet rs = pst.executeQuery();		

		
			while(rs.next()) {
				User user = new User();
				
				user.setUserId(rs.getInt("userid"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setFirstName(rs.getString("firstname"));
				user.setLastName(rs.getString("lastname"));
				user.setEmail(rs.getString("email"));
				user.setRole(new Role(rs.getInt("roleid"), rs.getString("role")));		
				users.add(user);
				
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return users;
	}

	@Override
	public List<User> getAllUsersByAccountType(int typeId) {
		List<User> users = new ArrayList<>();
		
		try {
			String sql = "SELECT * FROM users a \r\n" + 
					"FULL OUTER JOIN role b ON a.roleid = b.roleid \r\n" + 
					"FULL OUTER JOIN account c ON a.userid = c.userid \r\n" + 
					"FULL OUTER JOIN accountstatus d ON c.statusid = d.statusid \r\n" + 
					"INNER JOIN accounttype e ON c.typeid = e.typeid WHERE e.typeid = ?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, typeId);
			ResultSet rs = pst.executeQuery();		

		
			while(rs.next()) {
				User user = new User();
				
				user.setUserId(rs.getInt("userid"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setFirstName(rs.getString("firstname"));
				user.setLastName(rs.getString("lastname"));
				user.setEmail(rs.getString("email"));
				user.setRole(new Role(rs.getInt("roleid"), rs.getString("role")));

				
				users.add(user);
				
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return users;
	}

	@Override
	public List<User> getAllUsersByStatusType(int statusId) {
		List<User> users = new ArrayList<>();
		
		try {
			String sql = "SELECT * FROM users a\r\n" + 
					"FULL OUTER JOIN role b ON a.roleid = b.roleid \r\n" + 
					"FULL OUTER JOIN account c ON a.userid = c.userid \r\n" + 
					"FULL OUTER JOIN accounttype d ON c.typeid = d.typeid\r\n" + 
					"INNER JOIN accountstatus e ON c.statusid = e.statusid WHERE e.statusid = ?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, statusId);
			ResultSet rs = pst.executeQuery();		

		
			while(rs.next()) {
				User user = new User();
				
				user.setUserId(rs.getInt("userid"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setFirstName(rs.getString("firstname"));
				user.setLastName(rs.getString("lastname"));
				user.setEmail(rs.getString("email"));
				user.setRole(new Role(rs.getInt("roleid"), rs.getString("role")));				
				users.add(user);
				
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failed");
				e.printStackTrace();
		} 
		
		return users;
	}

	@Override
	public User getUserByUserId(int userId) {
		
		
		try {
			String sql = "SELECT * FROM users a \r\n" + 
					"INNER JOIN role b ON a.roleid = b.roleid \r\n" + 
					"INNER JOIN account c ON a.userid = c.userid \r\n" + 
					"INNER JOIN accountstatus d ON d.statusid = c.statusid \r\n" + 
					"INNER JOIN accounttype e ON e.typeid = c.typeid \r\n" + 
					"WHERE a.userid =?;";
				
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			ResultSet rs = pst.executeQuery();		

		
			if(rs.next()) {
				User user = new User();
				user.setUserId(rs.getInt("userid"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setFirstName(rs.getString("firstname"));
				user.setLastName(rs.getString("lastname"));
				user.setEmail(rs.getString("email"));
				user.setRole(new Role(rs.getInt("roleid"), rs.getString("role")));
				
				List<Account> accounts = new ArrayList<>();
				do {
					Account account = new Account(rs.getInt("accountid"), 
												  rs.getDouble("balance"), 
												  new AccountStatus(rs.getInt("statusid"), rs.getString("status")),
												  new AccountType(rs.getInt("typeid"), rs.getString("type")));
					accounts.add(account);
					
				} while(rs.next());
				
				user.setAccounts(accounts);				
				
				return user;
			}
			
		} catch (SQLException e) {
				System.out.println("Connection failedXXX");
				e.printStackTrace();
		} 
		
		return null;
	}

	@Override
	public boolean updateUser(User user) {

		try {
			String sql = "UPDATE users SET username=?, password=?, \r\n" + 
					"firstname=?, lastname=?, email=? WHERE userid=?;";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setString(1, user.getUsername());
			pst.setString(2, user.getPassword());
			pst.setString(3, user.getFirstName());
			pst.setString(4, user.getLastName());
			pst.setString(5, user.getEmail());
			pst.setInt(6, user.getUserId());
			
			pst.executeUpdate();
			return true;
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		return false;
	}

	@Override
	public int getUserId(String username, String password) {
		try {
			String sql = "SELECT userid FROM users WHERE username=? AND password=?";
			
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setString(1, username);
			pst.setString(2, password);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
				return rs.getInt("userid");
			
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return -1;		
	}

	@Override
	public boolean isAvailableUsername(String username) {
		try {
			String sql = "SELECT * FROM users WHERE username=?";
			
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, username);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
				return false;
			
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return true;
	}
	
	@Override
	public boolean isYourUsername(String username, int userId) {
		try {
			String sql = "SELECT username FROM users WHERE username=? AND userid=?";
			
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, username);
			pst.setInt(2, userId);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
				return true;
			
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return false;
	}
	
	

	@Override
	public boolean isAvailableEmail(String email) {
		try {
			String sql = "SELECT * FROM users WHERE email=?";
			
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, email);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
				return false;
			
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return true;
	}
	
	@Override
	public boolean isYourEmail(String email, int userId) {
		try {
			String sql = "SELECT email FROM users WHERE email=? AND userid=?";
			
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setString(1, email);
			pst.setInt(2, userId);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
				return true;
			
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return false;
	}

	@Override
	public boolean isCreateUser (User user) {
		
		try {
			String sql = "INSERT INTO users(username, password, firstName, lastName, email, roleid) "
					+ "VALUES(?,?,?,?,?,?)";
		
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			
			pst.setString(1, user.getUsername());
			pst.setString(2, user.getPassword());
			pst.setString(3, user.getFirstName());
			pst.setString(4, user.getLastName());
			pst.setString(5, user.getEmail());
			pst.setInt(6, user.getRole().getRoleId());
			
			pst.executeUpdate();
			
			return true;
		
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		return false;
	}

	@Override
	public String getRolebyUserId(int userId) {
		try {
			String sql = "SELECT role FROM role a \r\n" + 
					"INNER JOIN users b ON a.roleid = b.roleid WHERE userid=?";
			
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
				return rs.getString("role");
			
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return null;
	}
	
	@Override
	public String getRolebyRoleId(int roleId) {
		try {
			String sql = "SELECT role FROM role WHERE roleid=?;";
			
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, roleId);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
				return rs.getString("role");
			
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return null;
	}

	@Override
	public boolean isValidRoleId(int roleId) {
		try {
			String sql = "SELECT * FROM role WHERE roleId=?";
			
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, roleId);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
				return true;
			
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return false;
	}

	@Override
	public boolean hasUser(int userId) {
		try {
			String sql = "SELECT * FROM users WHERE userid=?";
			
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
				return true;
			
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return false;
	}

	@Override
	public String getUsername(int userId) {
		try {
			String sql = "SELECT username FROM users WHERE userid=?;";
			
			conn = DaoConnection.getConnection();
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next())
				return rs.getString("username");
			
		} catch (SQLException e) {
			System.out.println("Connection failed");
			e.printStackTrace();
		} 
		
		return null;
	}

}
