package com.bankingapi.dao;

import java.util.HashMap;
import java.util.List;

import com.bankingapi.model.User;

public interface UserDao {

	
	public boolean isCreateUser(User user);
	
	public HashMap<Integer, User> getAllUsers();
	public List<User> getAllUsersByRoleId(int roleId);
	public List<User> getAllUsersByAccountType(int typeId);
	public List<User> getAllUsersByStatusType(int statusId);
	public User getUserByUserId(int userId);
	public int getUserId(String username, String password);
	public String getUsername(int userId);
	
	public boolean updateUser(User user);

	
	public boolean isAvailableUsername(String username); 
	public boolean isAvailableEmail(String email);
	public boolean isValidRoleId(int roleId);
	public boolean hasUser(int userId);
	public boolean isYourEmail(String email, int userId);
	public boolean isYourUsername(String username, int userId);
	
	public String getRolebyUserId(int userId);
	public String getRolebyRoleId(int roleId);
	
	
}
