package com.bankingapi.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpRetryException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.model.User;
import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.servlethelper.UserRequestHelper;
import com.bankingapi.subservices.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserController {

	private static UserServices userServices = new UserServicesImpl();
	
	public static void getUser(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();
		int userId = (int) session.getAttribute("userId");
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);
		
		System.out.println("Find Users By Id User controller started");

		User searchUser = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			searchUser = mapper.readValue(req.getInputStream(), User.class);
			

		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		int searchId = searchUser.getUserId();			
		User user = userServices.getUser(searchId);
		
		if(user != null) {
			
			if (role.equals("Admin") || role.equals("Employee")) {				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(user));
			
			} else if (role.equals("Standard") && searchId == userId) {				
				resp.getWriter().write(new ObjectMapper().writeValueAsString(user));
				
			} else {				
				Message message = userServices.getMessage("The requested action is not permitted");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}			
		} else {			
			Message message = userServices.getMessage("There is no user in the system");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}			
	}		
	
	
	public static void getAllUser(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
			
		HttpSession session = req.getSession();
		int userId = (int) session.getAttribute("userId");
		resp.setContentType("application/json");
		String role = userServices.getUserRole(userId);		
		
		System.out.println("Find All Users - User controller started");
		
		if(role.equals("Admin") || role.equals("Employee")) {					
			HashMap<Integer, User> users = userServices.getAllUsers();
			
			if(users.size() != 0) {						
				resp.getWriter().write(new ObjectMapper().writeValueAsString(users));

			} else {				
				Message message = userServices.getMessage("no user in the system");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			}	
			
		} else {			
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}
		
	} 
	
	public static void addUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		resp.setContentType("application/json");
		
		System.out.println("Register/Add user - User controller started");
		
		User newUser = null;
		try {
			ObjectMapper mapper  = new ObjectMapper();		
			newUser = mapper.readValue(req.getInputStream(), User.class); //user with zero id
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		try {			
			boolean isUserRegistered = userServices.registerUser(newUser);
			
			if (isUserRegistered) {			
				
				resp.setStatus(HttpServletResponse.SC_CREATED);
				User user = userServices.getUser(newUser.getUsername(), newUser.getPassword()); //user with new id
				resp.getWriter().write(new ObjectMapper().writeValueAsString(user));
				
				System.out.println("New user created");
			} else {
				Message message = userServices.getMessage("Invalid fields");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}

		} catch (Exception e) {

			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
	
	public static void updateUser(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
				
		HttpSession session = req.getSession();	
		resp.setContentType("application/json");
		int userId = (int)session.getAttribute("userId");
		String role = userServices.getUserRole(userId);
		
		System.out.println("Update user - User controller started");
		
		User user = null;
		try {
			ObjectMapper mapper  = new ObjectMapper();		
			user = mapper.readValue(req.getInputStream(), User.class);	
			
		} catch (Exception e) {
			System.out.println("Class Not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			return;
		}
		
		if(role.equals("Admin")) {		
			
			boolean isUserUpdated = userServices.updateUser(user);
			
			if (isUserUpdated) {				
				User updatedUser = userServices.getUser(user.getUserId()); // updated user;
				resp.getWriter().write(new ObjectMapper().writeValueAsString(updatedUser));
				
				System.out.println("User updated");
				
			} else {
				Message message = userServices.getMessage("Invalid fields");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));			
			}
			
		} else if(userId == user.getUserId()) {
			
			boolean isUserUpdated = userServices.updateUser(user);
			
			if (isUserUpdated) {				
				User updatedUser = userServices.getUser(user.getUserId()); // updated user;
				resp.getWriter().write(new ObjectMapper().writeValueAsString(updatedUser));
				
				System.out.println("User updated");				
			} else {
				Message message = userServices.getMessage("Invalid fields");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));			
			}
			
		} else {
			Message message = userServices.getMessage("The requested action is not permitted");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
	
}
