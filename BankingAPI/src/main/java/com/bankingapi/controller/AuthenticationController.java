package com.bankingapi.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.model.User;
import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.subservices.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthenticationController {

	private static UserServices userServices = new UserServicesImpl();
	
	public static void login(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
			
		resp.setContentType("application/json");
		
		System.out.println("Login Authentication controller started");
		
		User user = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			user = mapper.readValue(req.getInputStream(), User.class);
		} catch (Exception e) {
			System.out.println("User class not created");
		}
		
		if(user == null) {
		
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			Message message = userServices.getMessage("Invalid Credentials");	
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			System.out.println("Invalid Credentials");
			
		} else {
			
			user = userServices.getUser(user.getUsername(), user.getPassword());
			
			if(user == null) {
				resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				Message message = userServices.getMessage("Invalid Credentials");	
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
				return;
			} 
			
			HttpSession session = req.getSession();
			session.setAttribute("userId", user.getUserId());
			resp.getWriter().write(new ObjectMapper().writeValueAsString(user));
			
			System.out.println("Session created");
			
		}		
	}
	
	public static void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		HttpSession session = req.getSession();	
		
		System.out.println("Logout Authentication controller started");
		
		if (session.getAttribute("userId") == null || !req.getMethod().equals("POST")) {
			
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			Message message = userServices.getMessage("There was no user logged into the session");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			System.out.println("There was no user logged into the session");
			
		} else {
			
			int userId = (int)session.getAttribute("userId");
			String username = userServices.getUsername(userId);
			
			String response = "You have successfully logged out {" + username + "}";
			
			Message message = userServices.getMessage(response);
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
			
			session.removeAttribute("userId");
			session.invalidate();
			
			System.out.println("Session closed successfully");
		}		
	}	
}
