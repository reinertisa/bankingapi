package com.bankingapi.services;

import java.util.List;

import com.bankingapi.model.Account;

public interface AccountServices {


	public boolean isAccountCreated(int userId, Account account);
	public boolean createCheckingAccount(int userId, int typeId);
	public boolean createSavingsAccount(int userId, int typeId);
	
	public List<Account> getAllAccounts();
	public Account getAccountByAccountId(int accountId);
	public List<Account> getAllAccountsByStatus(int statusId);
	public List<Account> getAccountsByUserId(int userId);
	
	
	public boolean hasEnoughBalanceForWithdrawal(int accountId, double balance);
	public boolean isMakeAWithdrawal(int accountId, double amount);
	public boolean makeAWithdrawal(int accountId, double amount);
	public boolean isMakeADeposit(int accountId, double amount);
	public boolean makeADeposit(int accountId, double amount);
	public boolean isTransferFunds(int sourceAccountId, int targetAccountId, double amount);
	public boolean makeTransferFunds(int sourceAccountId, int targetAccountId, double amount);
	
	public int getAccountIdByUserId(int userId);
	public int getUserIdInAccountByAccountId(int accountId);
//	public boolean isValidAccount(int accountId);
	public boolean isOpenAccount(int accountId, int userId);
	public boolean isOpenAccount(int accountId);
	public boolean isPendingAccount(int accountId);
	public boolean isAccountOwnedByUser(int accountId, int userId);
	public boolean hasAccount(int accountId);
	public int getLastAccountId(int userId);
	
	public boolean hasEnoughOpenAccounts(int userId);
	public boolean hasEnoughOpenAccountsForTransferMoneyInYourAccounts(int userId);
	public boolean isEqualAccountId(int firstAccount, int secondAccount);
	
	public boolean isAccountUpdated(int userId, Account account);
	public boolean updateAccountStatusOpen(int accountId);	
	public boolean updateAccountStatusDenied(int accountId);
	public void deleteAccount();
	
}
