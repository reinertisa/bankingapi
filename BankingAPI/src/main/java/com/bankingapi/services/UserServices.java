package com.bankingapi.services;

import java.util.HashMap;
import com.bankingapi.model.User;
import com.bankingapi.subservices.Message;

public interface UserServices {
	
	
	public boolean registerUser(User newUser);
	
	public User getUser(int userId);
	public User getUser(String username, String password);
	public HashMap<Integer, User> getAllUsers();
	public User getUser(String userId);
	public String getUserRole(int userId);
	public String getUsername(int userId);
	
	public boolean updateUser(User user);
	public boolean hasUser(int userId);
	
	public Message getMessage(String message);

}
