package com.bankingapi.services;

import java.util.HashMap;
import com.bankingapi.dao.AccountDao;
import com.bankingapi.dao.AccountDaoImpl;
import com.bankingapi.dao.UserDao;
import com.bankingapi.dao.UserDaoImpl;
import com.bankingapi.model.User;
import com.bankingapi.subservices.Message;
import com.bankingapi.subservices.UserValidationControl;


public class UserServicesImpl implements UserServices{

	private UserDao userDao = new UserDaoImpl();
	private AccountDao accountDao = new AccountDaoImpl();
	UserValidationControl control = new UserValidationControl();
	
	@Override
	public boolean registerUser(User newUser) {
		
		if(!control.isAvailableUsername(newUser.getUsername())) {
			return false;
		}
		
		if(!control.isValidPassword(newUser.getPassword())) {
			return false;
		}
		
		if(!control.isValidFirstName(newUser.getFirstName())) {
			return false;
		}
		
		if(!control.isValidLastName(newUser.getLastName())) {
			return false;
		}
		
		if(!control.isValidEmail(newUser.getEmail())) {
			return false;
		}
		
		if(!control.isValidRoleId(newUser.getRole().getRoleId())){
			return false;
		}
			
		if(userDao.isCreateUser(newUser)) {
			int userId = userDao.getUserId(newUser.getUsername(), newUser.getPassword());
			if(accountDao.isAccountCreated(userId, newUser.getAccounts().get(0))) {
				return true;
			} else {
				return false;
			}
			
		} else {
			return false;
		}

	}	
	
	@Override
	public User getUser(int userId) {
		
		if(userDao.hasUser(userId)) {
			return userDao.getUserByUserId(userId);
		}		
		return null;		
	}

	@Override
	public User getUser(String username, String password) {
		
		int userId = userDao.getUserId(username, password);
		if(userId == -1) {
			return null;
		} else {
			return userDao.getUserByUserId(userId);	
		}
				
	}


	@Override
	public HashMap<Integer, User> getAllUsers() {
		return userDao.getAllUsers();
	}

	@Override
	public User getUser(String searchId) {
		
		try {
			return userDao.getUserByUserId(Integer.parseInt(searchId));
		} catch (Exception e) {
			System.out.println("Exception: searchid not found in getUser");
		}
	
		return null;	
	}

	@Override
	public Message getMessage(String message) {		
		return new Message(message);
	}
	
	@Override
	public String getUserRole(int userId) {		
		return userDao.getRolebyUserId(userId);
	}

	@Override
	public boolean updateUser(User user) {
		
		if(!userDao.hasUser(user.getUserId())) {
			return false;
		}
		
		if(!userDao.isAvailableEmail(user.getEmail())) {
			if(!userDao.isYourEmail(user.getEmail(), user.getUserId())) {
				return false;
			}
		}
		
		if(!userDao.isAvailableUsername(user.getUsername())) {
			if(!userDao.isYourUsername(user.getUsername(), user.getUserId())) {
				return false;
			}
		}		
		
		return userDao.updateUser(user);		
	}

	@Override
	public boolean hasUser(int userId) {

		return userDao.hasUser(userId);
	}

	@Override
	public String getUsername(int userId) {
		return userDao.getUsername(userId);
	}


	
	
	
 }
 