package com.bankingapi.services;

public class Message {

	String message;

	public Message() {
		this.message = null;
	}
	
	public Message(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Message [message=" + message + "]";
	}
	
}
