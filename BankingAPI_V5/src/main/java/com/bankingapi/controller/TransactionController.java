package com.bankingapi.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankingapi.services.AccountServices;
import com.bankingapi.services.AccountServicesImpl;
import com.bankingapi.services.UserServices;
import com.bankingapi.services.UserServicesImpl;
import com.bankingapi.services.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TransactionController {
	
	private static AccountServices accountServices = new AccountServicesImpl();
	private static UserServices userServices = new UserServicesImpl();
	
	public static void withdrawal(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");		
		String role = userServices.getUserRole(userId);
		
		System.out.println("Withdraw transaction controller started");
		
		WithdrawAndDepositNode node = null;		
		try {			
			ObjectMapper mapper = new ObjectMapper();
			node = mapper.readValue(req.getInputStream(), WithdrawAndDepositNode.class);
			
		} catch (Exception e) {
			System.out.println("WithdrawAndDepositNode not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			return;
		} 
		
		int accountId = node.getAccountId();
		double amount = node.getAmount();
		
		try {

			if(role.equals("Admin") || accountServices.isAccountOwnedByUser(accountId, userId)) {
				
				String output = accountServices.isMakeAWithdrawal(accountId, amount);
				
				if(output.equals("success")) {
					
					accountServices.makeAWithdrawal(accountId, amount);
					
					String response = "$" + "{" + amount + "}" + 
									" has been withdrawn from Account #{" + accountId + "}";					
				
					Message message = userServices.getMessage(response);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
					
					System.out.println("Withdraw successful");
					
				} else {
					System.out.println("Invalid fields");
					Message message = userServices.getMessage(output);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
				}
								
			} else {
				System.out.println("Invalid fields");
				resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				Message message = userServices.getMessage("The requested action is not permitted");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));;
			}	
						
		} catch (Exception e) {
			System.out.println("Invalid fields");

			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}
	}
	
	
	public static void deposit(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");		
		String role = userServices.getUserRole(userId);
		
		System.out.println("Deposit transaction controller started");
		
		WithdrawAndDepositNode node = null;
		
		try {			
			ObjectMapper mapper = new ObjectMapper();
			node = mapper.readValue(req.getInputStream(), WithdrawAndDepositNode.class);
		
		} catch (Exception e) {
			System.out.println("WithdrawAndDepositNode not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			return;
		} 
		
		int accountId = node.getAccountId();
		double amount = node.getAmount();
		
		try {

			if(role.equals("Admin") || accountServices.isAccountOwnedByUser(accountId, userId)) {
				
				String output = accountServices.isMakeADeposit(accountId, amount);
				
				if(output.equals("success")) {
					
					accountServices.makeADeposit(accountId, amount);
						
					String response = "$" + "{" + amount + "}" + 
								" has been deposited to Account #{" + accountId + "}";
					
					Message message = userServices.getMessage(response);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
						
					System.out.println("Deposit successful");
						
				} else {
					System.out.println("Invalid fields");
					Message message = userServices.getMessage(output);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
				}
				
			} else {
				System.out.println("The requested action is not permitted");
				resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				Message message = userServices.getMessage("The requested action is not permitted");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));;
			}	
						
		} catch (Exception e) {
			System.out.println("Invalid fields");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
	}
	
	
	public static void transfer(HttpServletRequest req, HttpServletResponse resp) throws JsonProcessingException, IOException {
		
		HttpSession session = req.getSession();		
		int userId = (int)session.getAttribute("userId");		
		resp.setContentType("application/json");		
		String role = userServices.getUserRole(userId);
		
		System.out.println("Transfer transaction controller started");
		
		TransferNode node = null;
		
		try {			
			ObjectMapper mapper = new ObjectMapper();
			node = mapper.readValue(req.getInputStream(), TransferNode.class);
		
		} catch (Exception e) {
			System.out.println("TransferNode not created");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
			return;
		} 
		
		int sourceAccountId = node.getSourceAccountId();
		int targetAccountId = node.getTargetAccountId();
		double amount = node.getAmount();
		
		try {

			if(role.equals("Admin") || accountServices.isAccountOwnedByUser(sourceAccountId, userId)) {
				
				String output = accountServices.isTransferFunds(sourceAccountId, targetAccountId, amount);
				
				if(output.equals("success")) {
					
					accountServices.makeTransferFunds(sourceAccountId, targetAccountId, amount);				
					
					String response = "${" + node.getAmount() + "} has been transferred " +
								      "from Account #{" + node.getSourceAccountId() + "}" +
						              " to Account #{" + node.getTargetAccountId() + "}";
					
					Message message = userServices.getMessage(response);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
					
					System.out.println("Transfer successful");
				
				} else {
					System.out.println("Invalid fields");
			
					Message message = userServices.getMessage(output);
					resp.getWriter().write(new ObjectMapper().writeValueAsString(message));	
				}		
			} else {
				
				System.out.println("The requested action is not permitted");
				resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				Message message = userServices.getMessage("The requested action is not permitted");
				resp.getWriter().write(new ObjectMapper().writeValueAsString(message));;
			}
						
		} catch (Exception e) {
			System.out.println("Invalid fields");
			
			Message message = userServices.getMessage("Invalid fields");
			resp.getWriter().write(new ObjectMapper().writeValueAsString(message));
		}		
		
	}
}

class WithdrawAndDepositNode {
	int accountId;
	double amount;
	
	public WithdrawAndDepositNode() {

	}
	public WithdrawAndDepositNode(int accountId, double amount) {
		this.accountId = accountId;
		this.amount = amount;
	}
	
	public int getAccountId() {
		return accountId;
	}
	
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return "Money [accountId=" + accountId + ", amount=" + amount + "]";
	}
}

class TransferNode {
	
    int sourceAccountId;
    int targetAccountId;
    double amount;
	public TransferNode() {

	}
	
	public TransferNode(int sourceAccountId, int targetAccountId, double amount) {
	
		this.sourceAccountId = sourceAccountId;
		this.targetAccountId = targetAccountId;
		this.amount = amount;
	}

	public int getSourceAccountId() {
		return sourceAccountId;
	}

	public void setSourceAccountId(int sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}

	public int getTargetAccountId() {
		return targetAccountId;
	}

	public void setTargetAccountId(int targetAccountId) {
		this.targetAccountId = targetAccountId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "TransferNode [sourceAccountId=" + sourceAccountId + ", targetAccountId=" + targetAccountId + ", amount="
				+ amount + "]";
	}
	
	
}

